﻿using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.Windows;
using System.Windows.Input;

namespace ArphoxDIP
{
    public partial class MainWindow : Window
    {
        Histogram hisztogram { get; set; }
        System.Drawing.Bitmap Kep { get; set; }
        System.Drawing.Bitmap KepMasolat { get; set; }
        Info info;

        //Képernyő méretek, mert full screenben NaN a Width és Height
        double Width2 = SystemParameters.PrimaryScreenWidth;
        double Height2 = SystemParameters.PrimaryScreenHeight - GlobalInfo.TalcaMagassag;

        ParameterObject paramobj_incoming { get; set; }
        ParameterObject paramobj_outgoing { get; set; }

        public void Init()
        {
            InitializeComponent();

            this.DataContext = this;
            this.WindowStartupLocation = System.Windows.WindowStartupLocation.CenterScreen;
            this.WindowState = System.Windows.WindowState.Maximized;

            paramobj_outgoing = new ParameterObject();
        }

        //Paraméter nélküi hívás = első futás
        public MainWindow()
        {
            Init();
            info = new Info(this);
        }

        //Paraméteres hívás - új ablakként hívtuk meg
        public MainWindow(ParameterObject po)
        {
            Init();
            info = new Info(this);
            info.LoadTime = po.LoadTime;
            this.paramobj_incoming = po;

            info.LoadedImageExtension = null;
            info.LoadedImagePath = null;
            info.LoadedObjectType = po.ObjectType;
            info.LoadTime = po.LoadTime;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            if (paramobj_incoming != null) //Ha ParamObj-ból töltöttünk be
            {
                statusTime.Content = String.Format("{0} ms.", info.LoadTime);

                if (paramobj_incoming.ObjectType == ObjectType.Image)
                {
                    info.LoadedImageType = paramobj_incoming.ImageType;
                    info.LoadedHistogramType = HistogramType.Nothing;
                    this.Kep = paramobj_incoming.Image;
                    ParamLoad_Kep();
                }
                else //po.ObjectType == ObjectType.Histogram
                {
                    info.LoadedImageType = ImageType.Unknown;
                    info.LoadedHistogramType = paramobj_incoming.HistogramType;
                    this.hisztogram = paramobj_incoming.Histogram;

                    if (info.LoadedHistogramType == HistogramType.Grayscale)
                        ParamLoad_HiszGray();
                    else
                        ParamLoad_HiszRGB();
                }
                paramobj_incoming = null;
            }
            else //Tiszta betöltés
            {
                HideFunctions();
                statusTime.Content = String.Format("0 ms.");
            }
        }
        private void Window_KeyDown(object sender, KeyEventArgs e)
        {
            //Ctrl + Z-re visszaugrik az előző képre = bezárja az ablakot
            if (e.Key == Key.Z && e.KeyboardDevice.Modifiers == ModifierKeys.Control)
            {
                this.Close();
                return;
            }

            switch (e.Key)
            {
                case Key.Escape: this.Close(); return;
                case Key.T: info.TesztMod = !info.TesztMod; TesztModChecker.IsChecked = info.TesztMod; return;
                //Nyíl gombok: scrollozik 10%-ot
                case Key.Left: scroller.ScrollToHorizontalOffset(scroller.HorizontalOffset - scroller.ScrollableWidth * 0.1); return;
                case Key.Right: scroller.ScrollToHorizontalOffset(scroller.HorizontalOffset + scroller.ScrollableWidth * 0.1); return;
                case Key.Up: scroller.ScrollToVerticalOffset(scroller.VerticalOffset - scroller.ScrollableHeight * 0.1); return;
                case Key.Down: scroller.ScrollToVerticalOffset(scroller.VerticalOffset + scroller.ScrollableHeight * 0.1); return;
                //NumPad irány billentyűkkel ugorhatunk a scroll részeibe (NumLock legyen ON!)
                case Key.NumPad1: scroller.ScrollToLeftEnd(); scroller.ScrollToBottom(); return;
                case Key.NumPad2: scroller.ScrollToBottom(); scroller.ScrollToHorizontalOffset(image.Width / 2 - this.Width2 / 2); return;
                case Key.NumPad3: scroller.ScrollToRightEnd(); scroller.ScrollToBottom(); return;
                case Key.NumPad4: scroller.ScrollToLeftEnd(); scroller.ScrollToVerticalOffset(image.Height / 2 - this.Height2 / 2); return;
                case Key.NumPad5:
                    scroller.ScrollToHorizontalOffset(image.Width / 2 - this.Width2 / 2);
                    scroller.ScrollToVerticalOffset(image.Height / 2 - this.Height2 / 2); break;
                case Key.NumPad6: scroller.ScrollToRightEnd(); scroller.ScrollToVerticalOffset(image.Height / 2 - this.Height2 / 2); return;
                case Key.NumPad7: scroller.ScrollToLeftEnd(); scroller.ScrollToHome(); return;
                case Key.NumPad8: scroller.ScrollToTop(); scroller.ScrollToHorizontalOffset(image.Width / 2 - this.Width2 / 2); return;
                case Key.NumPad9: scroller.ScrollToHome(); scroller.ScrollToRightEnd(); return;
            }


            /* Az Application.Current.ShutdownMode adja meg, hogy mikor záródjon be az egész alkalmazás.
             * Mivel az Application.ShutdownMode default értéke OnLastWindowClose, nekünk ezt nem kell átállítani,
             * tökéletes lesz az, hogy akkor záródik be az alkalmazás, amikor az utolsó ablak. */
        }
        private void Window_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.MiddleButton == MouseButtonState.Pressed)
            {
                info.Zoom = 0;
                image.Width = Kep.Width;
                image.Height = Kep.Height;
            }
        }
        private void Window_PreviewMouseWheel(object sender, MouseWheelEventArgs e)
        {   /* Azért ezt hívom, mert ezzel megy. Sima MouseWheel utoljára hívódik meg, és szerintem
             * elveszi az eventet a ScrollViewer, vagy valami ilyesmi. Szívtam már vele. Viszont mivel
             * ez hívódik meg először (Preview Tunnel, majd Sima event Bubble), tutira elkapja. */
            double zooooom = 1 + GlobalInfo.ZoomMertek;
            if (e.Delta > 0) //Felfelé görget
            {
                image.Width = image.Width * zooooom;
                image.Height = image.Height * zooooom;
                info.Zoom++;
            }
            else            //Lefelé görget
            {
                image.Width = image.Width / zooooom;
                image.Height = image.Height / zooooom;
                info.Zoom--;
            }
        }

        private unsafe void image_MouseMove(object sender, MouseEventArgs e)
        {
            if (info.Zoom == 0)
            {
                System.Windows.Point pont = e.GetPosition(image);
                //Valamilyen csoda folytán egy 50x50-es képnél 50x50-es image méretnél is simán képes 50-es értéket visszaadni (úgy, hogy 0-tól indexel),
                //így ha túlszalad azon, amit be tudunk indexelni, le kell vágni az utolsó pixelre. Extrém eset, de megtörténhet.
                int xpos = (pont.X >= Kep.Height) ? Kep.Height - 1 : (int)pont.X;
                int ypos = (pont.Y >= Kep.Width) ? Kep.Width - 1 : (int)pont.Y;

                //NEM HISZEM EL HOGY EZ MIÉRT INDEXEL TÚÚÚÚÚÚÚÚÚÚÚÚÚÚÚL
                //int xpos = (int)pont.X;
                //int ypos = (int)pont.Y;


                //Ez is működik, de nem gyorsít rajta (mindkettőre <= 1 ms sebességet mond a VS15 diagnosztika), szóval maradok az egyszerűbb megoldásnál.
                //BitmapData imageInData = Kep.LockBits(new Rectangle(0, 0, Kep.Width, Kep.Height), ImageLockMode.ReadOnly, PixelFormat.Format24bppRgb);
                //int offset = imageInData.Stride - Kep.Width * 3;
                //byte* p = (byte*)imageInData.Scan0;
                //p += imageInData.Stride * ypos + xpos * 3;
                //statusValues.Content = String.Format("X: {0}\tY: {1}\tR: {2}\tG: {3}\tB: {4}", xpos, ypos, p[2], p[1], p[0]);
                //Kep.UnlockBits(imageInData);

                System.Drawing.Color color = Kep.GetPixel(xpos, ypos);
                statusValues.Content = String.Format("X: {0}\tY: {1}\tR: {2}\tG: {3}\tB: {4}", xpos, ypos, color.R, color.G, color.B);
            }
        }

        public void ChangeBackgroundToTest()
        {
            this.Background = System.Windows.Media.Brushes.Aqua;
        }
        public void ChangeBackgroundToNormal()
        {
            this.Background = System.Windows.Media.Brushes.Bisque;
        }
    }
}