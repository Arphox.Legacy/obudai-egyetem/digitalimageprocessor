﻿using System;
using System.Drawing;
using System.IO;

namespace ArphoxDIP
{
    static class FileOperations
    {
        /// <summary>
        /// A path-ról betölt egy hisztogram fájlt és visszaadja egy long[] tömbként.
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        public static double[] LoadHistogram(string path)
        {
            double[] histogram = new double[256];

            StreamReader sr = new StreamReader(path);

            for (int i = 0; i < 256; i++)
            {
                histogram[i] = double.Parse(sr.ReadLine());
            }

            sr.Close();
            sr.Dispose();

            return histogram;
        }

        /// <summary>
        /// Kimenti a path-ra a long[] histogramot
        /// </summary>
        /// <param name="histogram"></param>
        /// <param name="path"></param>
        public static void SaveHistogram(double[] histogram, string path)
        {
            StreamWriter sw = new StreamWriter(path);

            for (int i = 0; i < histogram.Length; i++)
                sw.WriteLine(histogram[i].ToString());

            sw.Close();
            sw.Dispose();
        }
    }
}