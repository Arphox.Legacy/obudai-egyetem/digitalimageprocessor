﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.Linq;
using System.Text;


namespace ArphoxDIP
{
    public static class ImageProcessor
    {
        public static class PontMuveletek
        {
            public static unsafe Bitmap Invertalas(Bitmap imageIn)
            {
                Bitmap imageOut = new Bitmap(imageIn);
                //LUT készítése: LUT[i] = 255 - i;
                byte[] lut = new byte[256];
                for (int i = 0; i <= 255; i++)  //sajnos i nem lehet byte, mert 255 után 0-ra fordul és végtelen ciklus lesz belőle
                    lut[i] = (byte)~i;

                int W = imageOut.Width;
                int H = imageOut.Height;

                BitmapData bmData = imageOut.LockBits(new Rectangle(0, 0, W, H),
                                               ImageLockMode.ReadWrite,
                                               PixelFormat.Format24bppRgb);

                int stride = bmData.Stride;     //tényleges sorhossz a memóriában
                byte* p = (byte*)bmData.Scan0;  //pont-től indul a kép a memóriában

                int offset = stride - W * 3;   //* 3, mert (R + G + B) = 3 byte

                for (int y = 0; y < H; y++)
                {
                    for (int x = 0; x < W; x++)
                    {
                        p[0] = lut[p[0]]; //R
                        p[1] = lut[p[1]]; //G
                        p[2] = lut[p[2]]; //B

                        p += 3; //pointer arrébb rakása 3 byte-al, így a következő pixelbe érünk az adott sorban
                    }
                    p += offset; //ha végeztünk a sorral, ugrás a kövi sor első pixelére (ha (kep.Width * 3 == stride), akkor pont += 0)
                }

                imageOut.UnlockBits(bmData);
                return imageOut;
            }

            public static unsafe Bitmap Grayscale(Bitmap imageIn)
            {
                Bitmap imageOut = new Bitmap(imageIn);
                int W = imageOut.Width;
                int H = imageOut.Height;
                BitmapData bmData = imageOut.LockBits(new Rectangle(0, 0, W, H),
                                               ImageLockMode.ReadWrite,
                                               PixelFormat.Format24bppRgb);
                int stride = bmData.Stride, offset = stride - W * 3;
                byte* p = (byte*)bmData.Scan0;
                byte value = 0;

                for (int y = 0; y < H; y++)
                {
                    for (int x = 0; x < W; x++)
                    {
                        value = (byte)(p[0] * 0.299 + p[1] * 0.587 + p[2] * 0.114);
                        p[0] = value;
                        p[1] = value;
                        p[2] = value;

                        p += 3;
                    }
                    p += offset;
                }

                imageOut.UnlockBits(bmData);
                return imageOut;
            }

            public static unsafe Bitmap IntenzitasSzintreVagas_Normal(Bitmap imageIn, byte low, byte high)
            {
                Bitmap imageOut = new Bitmap(imageIn);
                int W = imageOut.Width;
                int H = imageOut.Height;
                BitmapData bmData = imageOut.LockBits(new Rectangle(0, 0, W, H),
                                               ImageLockMode.ReadWrite,
                                               PixelFormat.Format24bppRgb);
                int stride = bmData.Stride;
                byte* p = (byte*)bmData.Scan0;

                int offset = stride - W * 3;

                for (int y = 0; y < H; y++)
                {
                    for (int x = 0; x < W; x++)
                    {
                        p[0] = p[0] >= low && p[0] <= high ? (byte)255 : p[0];
                        p[1] = p[1] >= low && p[1] <= high ? (byte)255 : p[1];
                        p[2] = p[2] >= low && p[2] <= high ? (byte)255 : p[2];

                        p += 3;
                    }
                    p += offset;
                }

                imageOut.UnlockBits(bmData);
                return imageOut;
            }
            public static unsafe Bitmap IntenzitasSzintreVagas_DuplaKuszoboles(Bitmap imageIn, byte low, byte high)
            {
                Bitmap imageOut = new Bitmap(imageIn);
                int W = imageOut.Width;
                int H = imageOut.Height;
                BitmapData bmData = imageOut.LockBits(new Rectangle(0, 0, W, H),
                                               ImageLockMode.ReadWrite,
                                               PixelFormat.Format24bppRgb);
                int stride = bmData.Stride;
                byte* p = (byte*)bmData.Scan0;
               
                int offset = stride - W * 3;

                for (int y = 0; y < H; y++)
                {
                    for (int x = 0; x < W; x++)
                    {  //gyorsabbnak bizonyult a kasztolgatás, mint ha kiraknám külön változóba
                        p[0] = p[0] >= low && p[0] <= high ? (byte)255 : (byte)0;
                        p[1] = p[1] >= low && p[1] <= high ? (byte)255 : (byte)0;
                        p[2] = p[2] >= low && p[2] <= high ? (byte)255 : (byte)0;

                        p += 3;
                    }
                    p += offset;
                }

                imageOut.UnlockBits(bmData);
                return imageOut;
            }

            public static unsafe Bitmap KontrasztNyujtasBinaris(Bitmap imageIn, byte M) //Küszöbölés
            {
                //0 M alatt, 255 M fölött

                Bitmap imageOut = new Bitmap(imageIn);
                int W = imageOut.Width;
                int H = imageOut.Height;
                BitmapData bmData = imageOut.LockBits(new Rectangle(0, 0, W, H),
                                               ImageLockMode.ReadWrite,
                                               PixelFormat.Format24bppRgb);
                int stride = bmData.Stride;
                byte* p = (byte*)bmData.Scan0;

                int offset = stride - W * 3;

                for (int y = 0; y < H; y++)
                {
                    for (int x = 0; x < W; x++)
                    {
                        p[0] = p[0] >= M ? (byte)255 : (byte)0;
                        p[1] = p[1] >= M ? (byte)255 : (byte)0;
                        p[2] = p[2] >= M ? (byte)255 : (byte)0;

                        p += 3;
                    }
                    p += offset;
                }

                imageOut.UnlockBits(bmData);
                return imageOut;
            }
            public static unsafe Bitmap KontrasztNyujtasSkalazo(Bitmap imageIn, byte M, double skala)
            {
                //sötétítés M alatt, világosítás M fölött
                Bitmap imageOut = new Bitmap(imageIn);
                skala = skala / 5000;
                int W = imageOut.Width;
                int H = imageOut.Height;
                BitmapData bmData = imageOut.LockBits(new Rectangle(0, 0, W, H),
                                               ImageLockMode.ReadWrite,
                                               PixelFormat.Format24bppRgb);

                int stride = bmData.Stride, offset = stride - W * 3;
                byte* p = (byte*)bmData.Scan0;

                int d = 0;
                double value = 0;

                for (int y = 0; y < H; y++)
                {
                    for (int x = 0; x < W; x++)
                    {
                        //R
                        d = p[0] - M;   //Ha Diff pozitív, akkor a felső ágon vagyunk, ha negatív akkor az alsó ágon
                        if (d >= 0)
                        {
                            value = (p[0] + (d * d * skala));
                            p[0] = (byte)(value > 255 ? 255 : value);
                        }
                        else
                        {
                            value = (p[0] - (d * d * skala));
                            p[0] = (byte)(value < 0 ? 0 : value);
                        }
                        //G
                        d = p[1] - M;
                        if (d >= 0)
                        {
                            value = (p[1] + (d * d * skala));
                            p[1] = (byte)(value > 255 ? 255 : value);
                        }
                        else
                        {
                            value = (p[1] - (d * d * skala));
                            p[1] = (byte)(value < 0 ? 0 : value);
                        }
                        //B
                        d = p[2] - M;
                        if (d >= 0)
                        {
                            value = (p[2] + (d * d * skala));
                            p[2] = (byte)(value > 255 ? 255 : value);
                        }
                        else
                        {
                            value = (p[2] - (d * d * skala));
                            p[2] = (byte)(value < 0 ? 0 : value);
                        }
                        p += 3;
                    }
                    p += offset;
                }

                imageOut.UnlockBits(bmData);
                return imageOut;
            }

            public static unsafe Bitmap KontrasztAllitas(Bitmap imageIn, double contrast)
            {
                //http://www.codeproject.com/Articles/33838/Image-Processing-using-C nagy mértékben módosítva és átírva unsafe kódra, lutolva

                Bitmap imageOut = new Bitmap(imageIn);
                int W = imageOut.Width;
                int H = imageOut.Height;
                BitmapData bmData = imageOut.LockBits(new Rectangle(0, 0, W, H),
                                               ImageLockMode.ReadWrite,
                                               PixelFormat.Format24bppRgb);

                int stride = bmData.Stride, offset = stride - W * 3;
                byte* p = (byte*)bmData.Scan0;

                contrast = (100.0 + contrast) / 100.0;  // Skálázás 0.0 és 2.0 közé
                contrast *= contrast;                   // Négyzetre emelés

                double[] lut = new double[256];
                for (int i = 0; i < 256; i++)
                {
                    lut[i] = i / 255.0 - 0.5;
                    lut[i] *= contrast;
                    lut[i] += 0.5;
                    lut[i] *= 255;
                    lut[i] = lut[i] < 0 ? 0 : lut[i];
                    lut[i] = lut[i] > 255 ? 255 : lut[i];
                }

                for (int i = 0; i < W; i++)
                {
                    for (int j = 0; j < H; j++)
                    {
                        p[0] = (byte)lut[p[0]];
                        p[1] = (byte)lut[p[1]];
                        p[2] = (byte)lut[p[2]];

                        p += 3;
                    }
                    p += offset;
                }
                imageOut.UnlockBits(bmData);
                return imageOut;
            }

            public static unsafe Bitmap LogaritmikusSkalazas(Bitmap imageIn, double logbase, double constant)
            {
                Bitmap imageOut = new Bitmap(imageIn);
                int W = imageOut.Width;
                int H = imageOut.Height;
                double[] lut = new double[256];
                for (int i = 0; i < 256; i++)
                {
                    lut[i] = constant * Math.Log(i, logbase);
                    lut[i] = lut[i] > 255 ? 255 : lut[i];
                    lut[i] = lut[i] < 0 ? 0 : lut[i];
                }

                BitmapData bmData = imageOut.LockBits(new Rectangle(0, 0, W, H),
                               ImageLockMode.ReadWrite,
                               PixelFormat.Format24bppRgb);

                int stride = bmData.Stride, offset = stride - W * 3;
                byte* p = (byte*)bmData.Scan0;

                for (int y = 0; y < H; y++)
                {
                    for (int x = 0; x < W; x++)
                    {
                        p[0] = (byte)lut[p[0]];
                        p[1] = (byte)lut[p[1]];
                        p[2] = (byte)lut[p[2]];

                        p += 3;
                    }
                    p += offset;
                }

                imageOut.UnlockBits(bmData);
                return imageOut;
            }

            public static unsafe Bitmap GammaKorrekcio(Bitmap imageIn, double number)
            {
                Bitmap imageOut = new Bitmap(imageIn);
                int W = imageOut.Width;
                int H = imageOut.Height;
                BitmapData bmData = imageOut.LockBits(new Rectangle(0, 0, W, H),
                               ImageLockMode.ReadWrite,
                               PixelFormat.Format24bppRgb);

                int stride = bmData.Stride, offset = stride - W * 3;
                byte* p = (byte*)bmData.Scan0;


                byte[] gamma = new byte[256];
                double x = 1.0 / number;
                for (int i = 0; i < 256; ++i)
                    gamma[i] = (byte)Math.Min(255, (int)((255.0 * Math.Pow(i / 255.0, x)) + 0.5)); //Math.Min gyorsabb mint az inline if

                for (int i = 0; i < W; i++)
                {
                    for (int j = 0; j < H; j++)
                    {
                        p[0] = gamma[p[0]];
                        p[1] = gamma[p[1]];
                        p[2] = gamma[p[2]];
                        p += 3;
                    }
                    p += offset;
                }

                imageOut.UnlockBits(bmData);
                return imageOut;
            }

            public static unsafe Bitmap BitSikonkentiVagas(Bitmap imageIn, byte sik)
            {
                //Csak grayscale képen értelmezett.
                //Jónak tűnik, de nem vagyok benne biztos.

                Bitmap imageOut = new Bitmap(imageIn);
                int W = imageOut.Width;
                int H = imageOut.Height;
                BitmapData bmData = imageOut.LockBits(new Rectangle(0, 0, W, H),
                               ImageLockMode.ReadWrite,
                               PixelFormat.Format24bppRgb);

                int stride = bmData.Stride, offset = stride - W * 3;
                byte* p = (byte*)bmData.Scan0;

                sik = (byte)(8 - sik);

                StringBuilder sb = new StringBuilder("00000000");
                sb[sik] = '1';
                byte mask = Convert.ToByte(sb.ToString(), 2); //Vegyük az sb.ToString()-et kettes számrendszerbeli számként és rakjuk be a byte-ba.

                for (int y = 0; y < H; y++)
                {
                    for (int x = 0; x < W; x++)
                    {
                        p[0] = (byte)(p[0] & mask);
                        if (p[0] > 0) p[0] = 255;
                        else p[0] = 0;

                        p[2] = p[1] = p[0];
                        p += 3;
                    }
                    p += offset;
                }

                imageOut.UnlockBits(bmData);
                return imageOut;
            }

            public static unsafe Bitmap Hibadiffuzio(Bitmap imageIn)
            {
                //Valamiért nem megy úgy, ahogy kéne, pedig próbáltam követni a pdf-ben leírt algoritmusokat
                //CSAK grayscale képre értelmezett!

                Bitmap imageOut = new Bitmap(imageIn);
                int W = imageOut.Width;
                int H = imageOut.Height;
                BitmapData bmData = imageOut.LockBits(new Rectangle(0, 0, W, H),
                               ImageLockMode.ReadWrite,
                               PixelFormat.Format24bppRgb);

                int stride = bmData.Stride, offset = stride - W * 3;
                byte* p = (byte*)bmData.Scan0;

                byte korabbi_ertek, ujertek;

                for (int y = 0; y < H; y++)
                {
                    for (int x = 0; x < W; x++)
                    {
                        //korábbi_érték := pixel[x][y]
                        korabbi_ertek = p[0];

                        //új_érték := A_legközelebbi_érték_a_cél_színskáláján (korábbi_érték)
                        ujertek = korabbi_ertek > 127 ? (byte)255 : (byte)0;

                        //pixel[x][y] := új_érték
                        p[0] = ujertek == 255 ? (byte)255 : (byte)0;
                        p[2] = p[1] = p[0]; //R,G,B mindhárom szín azonos legyen

                        //hiba := korábbi_érték - új_érték
                        int hiba = (int)korabbi_ertek - (int)ujertek;     //Ha nem kasztolom input-ot is shortba, akkor nem azt kapom amit akarok

                        //pixel[x+1][y] := pixel[x+1][y] + 7/16 * hiba
                        if (x < W - 1)
                            p[4] = (p[3] + (double)7 / 16 * hiba) > 127.0 ? (byte)255 : (byte)0;

                        //pixel[x-1][y+1] := pixel[x-1][y+1] + 3/16 * hiba
                        if (x > 0 && y < H - 1)
                            p[W - 3] = (p[W - 3] + (double)3 / 16 * hiba) > 127.0 ? (byte)255 : (byte)0;

                        //pixel[x][y+1] := pixel[x][y+1] + 5/16 * hiba
                        if (y < H - 1)
                            p[W] = (p[W] + (double)5 / 16 * hiba) > 127.0 ? (byte)255 : (byte)0;

                        //pixel[x+1][y+1] := pixel[x+1][y+1] + 1/16 * hiba
                        if (y < H - 1 && x < W - 1)
                            p[W + 3] = (p[W + 3] + (double)1 / 16 * hiba) > 127.0 ? (byte)255 : (byte)0;

                        p += 3;
                    }
                    p += offset;
                }

                imageOut.UnlockBits(bmData);
                return imageOut;
            }
        }

        public static class HisztogramMuveletek
        {
            public static Histogram NormalHisztogram(Bitmap imageIn, ImageType it)
            {
                if (it == ImageType.Grayscale)
                {
                    return new GrayscaleHistogram(HisztogramGenerator(imageIn, 1));
                }
                else
                {
                    double[] r = HisztogramGenerator(imageIn, 1);
                    double[] g = HisztogramGenerator(imageIn, 2);
                    double[] b = HisztogramGenerator(imageIn, 3);
                    return new RGBHistogram(r, g, b);
                }
            }
            private static unsafe double[] HisztogramGenerator(Bitmap imageIn, int layer) //Layer = 1,2,3 (R,G,B) (0 if grayscale)
            {
                double[] data = new double[256];

                BitmapData bmData = imageIn.LockBits(new Rectangle(0, 0, imageIn.Width, imageIn.Height),
                               ImageLockMode.ReadOnly,
                               PixelFormat.Format24bppRgb);
                int stride = bmData.Stride, offset = stride - imageIn.Width * 3;
                byte* p = (byte*)bmData.Scan0;

                if (layer == 0 || layer == 1)
                {
                    for (int y = 0; y < imageIn.Height; y++)
                    {
                        for (int x = 0; x < imageIn.Width; x++)
                        {
                            data[p[0]]++;
                            p += 3;
                        }
                        p += offset;
                    }
                    imageIn.UnlockBits(bmData);
                    return data;
                }
                else if (layer == 2)
                {
                    for (int y = 0; y < imageIn.Height; y++)
                    {
                        for (int x = 0; x < imageIn.Width; x++)
                        {
                            data[p[1]]++;
                            p += 3;
                        }
                        p += offset;
                    }
                    imageIn.UnlockBits(bmData);
                    return data;
                }
                else if (layer == 3)
                {
                    for (int y = 0; y < imageIn.Height; y++)
                    {
                        for (int x = 0; x < imageIn.Width; x++)
                        {
                            data[p[2]]++;
                            p += 3;
                        }
                        p += offset;
                    }
                    imageIn.UnlockBits(bmData);
                    return data;
                }
                else
                {
                    imageIn.UnlockBits(bmData);
                    throw new ArgumentException("Layer argument can only be 0,1,2 or 3.");
                }
            }

            public static Histogram HalmozottHisztogram(Bitmap imageIn, ImageType it)
            {
                if (it == ImageType.Grayscale)
                {
                    return new GrayscaleHistogram(HalmozottHisztogramGenerator(imageIn, 0));
                }
                else
                {
                    double[] r = HalmozottHisztogramGenerator(imageIn, 1);
                    double[] g = HalmozottHisztogramGenerator(imageIn, 2);
                    double[] b = HalmozottHisztogramGenerator(imageIn, 3);
                    return new RGBHistogram(r, g, b);
                }
            }
            private static double[] HalmozottHisztogramGenerator(Bitmap imageIn, int layer) //Layer = 1,2,3 (R,G,B) (0 if grayscale)
            {
                double[] data = new double[256];
                data = HisztogramGenerator(imageIn, layer);

                for (int i = 1; i < 256; i++)
                    data[i] = data[i - 1] + data[i];

                return data;
            }

            public static Histogram NormalizaltHisztogram(Bitmap imageIn, ImageType it)
            {
                double size = imageIn.Width * imageIn.Height;
                if (it == ImageType.Grayscale)
                {
                    double[] data = HisztogramGenerator(imageIn, 0);
                    for (int i = 0; i < 256; i++)
                        data[i] /= size;
                    return new GrayscaleHistogram(data);
                }
                else
                {
                    double[] r = HisztogramGenerator(imageIn, 1);
                    double[] g = HisztogramGenerator(imageIn, 2);
                    double[] b = HisztogramGenerator(imageIn, 3);
                    for (int i = 0; i < 256; i++)
                    {
                        r[i] /= size;
                        g[i] /= size;
                        b[i] /= size;
                    }
                    return new RGBHistogram(r, g, b);
                }
            }

            public static Histogram HalmozottNormalizaltHisztogram(Bitmap imageIn, ImageType it)
            {
                if (it == ImageType.Grayscale)
                {
                    GrayscaleHistogram hist = (GrayscaleHistogram)NormalizaltHisztogram(imageIn, it);
                    for (int i = 1; i < 256; i++)
                        hist.Data[i] = hist.Data[i - 1] + hist.Data[i];
                    return hist;
                }
                else // ImageType.RGB
                {
                    RGBHistogram hist = (RGBHistogram)NormalizaltHisztogram(imageIn, it);
                    for (int i = 1; i < 256; i++)
                    {
                        hist.R[i] = hist.R[i - 1] + hist.R[i];
                        hist.G[i] = hist.G[i - 1] + hist.G[i];
                        hist.B[i] = hist.B[i - 1] + hist.B[i];
                    }
                    return hist;
                }
            }

            public static Bitmap HisztogramSzethuzas(Bitmap imageIn)
            {
                // HIBA:
                // Néha előjön olyan, hogy egyszerűen nem jelenik meg a készített kép. Jól csinálja, csak valamiért a 
                // nyíló ablakban nem látszik a kép.
                // Előidézés:   nyisd meg a chestxray.tif-et. Nem megy ha meghívod.
                //          DE! nyisd meg a leaf-et, majd a chestxray.tif-et. Most már megy. 
                // ??? :/

                //Jelenleg Grayscale képekre megy csak

                return InnerSzethuzas(imageIn, -1, -1); //ezzel jelezzük, hogy hisztogram széthúzást csináltatunk vele
            }
            public static Bitmap KontrasztSzethuzas(Bitmap imageIn, byte low, byte high)
            {
                return InnerSzethuzas(imageIn, low, high);
            }
            private static unsafe Bitmap InnerSzethuzas(Bitmap imageIn, int low, int high)
            {
                // Csak grayscale képre megy!
                Bitmap imageOut = new Bitmap(imageIn);
                bool hisztogramSzethuzas = false;
                int W = imageOut.Width;
                int H = imageOut.Height;
                BitmapData bmData = imageOut.LockBits(new Rectangle(0, 0, W, H),
                               ImageLockMode.ReadWrite,
                               PixelFormat.Format24bppRgb);
                int stride = bmData.Stride, offset = stride - W * 3;
                byte* p = (byte*)bmData.Scan0;

                if (low == -1 && high == -1) //csak kódból tudunk ilyet hívni mert a publikus kontraszt széthúzás byte-ot kap. Pici trükk :P
                    hisztogramSzethuzas = true;

                int minInt = 256, maxInt = -1;   //Intenzitások minimuma és maximuma (muszáj kezdőértéket adni mert sír a fordító)
                double a = 0;

                if (hisztogramSzethuzas)
                {
                    //Intenzitás minimum és maximum meghatározása:
                    for (int y = 0; y < H; y++)
                    {
                        for (int x = 0; x < W; x++)
                        {
                            if (p[0] < minInt)
                                minInt = p[0];
                            if (p[0] > maxInt)
                                maxInt = p[0];

                            p += 3;
                        }
                        p += offset;
                    }
                }
                else
                {
                    minInt = low;
                    maxInt = high;
                }

                int diff = maxInt - minInt;
                p = (byte*)bmData.Scan0;
                for (int y = 0; y < H; y++)
                {
                    for (int x = 0; x < W; x++)
                    {
                        a = p[0] - minInt;
                        p[0] = p[1] = p[2] = (byte)((a / diff) * 255);
                        p += 3;
                    }
                    p += offset;
                }

                imageOut.UnlockBits(bmData);
                return imageOut;
            }

            public static unsafe Bitmap HisztogramKiegyenlites(Bitmap imageIn)
            {
                Bitmap imageOut = new Bitmap(imageIn);
                //Csak grayscale képre megy!
                //http://sonabstudios.blogspot.in/2011/01/histogram-equalization-algorithm.html átírva
                int W = imageOut.Width;
                int H = imageOut.Height;
                BitmapData bmData = imageOut.LockBits(new Rectangle(0, 0, W, H),
                               ImageLockMode.ReadWrite,
                               PixelFormat.Format24bppRgb);
                int stride = bmData.Stride, offset = stride - W * 3;
                byte* p = (byte*)bmData.Scan0;

                double c = 255.0 / ((double)H * W);

                double[] histR = HalmozottHisztogramGenerator(imageIn, 1);
                double[] histG = HalmozottHisztogramGenerator(imageIn, 2);
                double[] histB = HalmozottHisztogramGenerator(imageIn, 3);
                int x, y;
                for (y = 0; y < H; y++)
                {
                    for (x = 0; x < W; x++)
                    {
                        p[0] = (byte)(histR[p[0]] * c);
                        p[1] = (byte)(histG[p[1]] * c);
                        p[2] = (byte)(histB[p[2]] * c);

                        p += 3;
                    }
                    p += offset;
                }

                imageOut.UnlockBits(bmData);
                return imageOut;
            }
        }

        public static class EgeszKepesMuveletek
        {
            public static unsafe Bitmap AND(Bitmap original, Bitmap mask)
            {
                //Csak grayscale-re megy!
                Bitmap imageOut = new Bitmap(original);
                int W = imageOut.Width;
                int H = imageOut.Height;
                Rectangle r = new Rectangle(0, 0, W, H);

                BitmapData originalData = imageOut.LockBits(r, ImageLockMode.ReadWrite, PixelFormat.Format24bppRgb);
                int stride = originalData.Stride, offset = stride - W * 3;
                byte* po = (byte*)originalData.Scan0;

                BitmapData maskData = mask.LockBits(r, ImageLockMode.ReadOnly, PixelFormat.Format24bppRgb);
                int stride2 = maskData.Stride, offset2 = stride - W * 3;
                byte* pm = (byte*)maskData.Scan0;

                for (int y = 0; y < mask.Height; y++)
                {
                    for (int x = 0; x < mask.Width; x++)
                    {
                        if (pm[0] != 255) //Ha más, akkor fekete színű lesz.
                        {
                            po[0] = 0;
                            po[1] = 0;
                            po[2] = 0;
                        }

                        po += 3;
                        pm += 3;
                    }
                    po += offset;
                    pm += offset2;
                }

                mask.UnlockBits(maskData);
                imageOut.UnlockBits(originalData);
                return imageOut;
            }
            public static unsafe Bitmap OR(Bitmap original, Bitmap mask)
            {
                //Csak grayscale-re megy!
                Bitmap imageOut = new Bitmap(original);
                int W = imageOut.Width;
                int H = imageOut.Height;
                Rectangle r = new Rectangle(0, 0, W, H);

                BitmapData originalData = imageOut.LockBits(r, ImageLockMode.ReadWrite, PixelFormat.Format24bppRgb);
                int stride = originalData.Stride, offset = stride - W * 3;
                byte* po = (byte*)originalData.Scan0;

                BitmapData maskData = mask.LockBits(r, ImageLockMode.ReadOnly, PixelFormat.Format24bppRgb);
                int stride2 = maskData.Stride, offset2 = stride - W * 3;
                byte* pm = (byte*)maskData.Scan0;

                for (int y = 0; y < H; y++)
                {
                    for (int x = 0; x < W; x++)
                    {
                        if (pm[0] != 0) //Ha más, akkor valami speciális színű lesz.
                        {
                            po[0] = 0xCC;
                            po[1] = 0xCC;
                            po[2] = 0x22;
                        }

                        pm += 3;
                        po += 3;
                    }
                    po += offset;
                    pm += offset2;
                }

                mask.UnlockBits(maskData);
                imageOut.UnlockBits(originalData);
                return imageOut;
            }
            public static unsafe Bitmap AVG(List<Bitmap> bemenetek)
            {
                Bitmap imageOut = new Bitmap(bemenetek[0].Width, bemenetek[0].Height, bemenetek[0].PixelFormat);
                int W = imageOut.Width;
                int H = imageOut.Height;
                Rectangle r = new Rectangle(0, 0, W, H);

                BitmapData bmData = imageOut.LockBits(r, ImageLockMode.ReadWrite, PixelFormat.Format24bppRgb);
                int stride = bmData.Stride, offset = stride - W * 3;
                byte* p = (byte*)bmData.Scan0;

                BitmapData[] bemenetDatas = new BitmapData[bemenetek.Count];
                byte*[] bemenetPointers = new byte*[bemenetek.Count];
                for (int i = 0; i < bemenetDatas.Length; i++)
                {
                    bemenetDatas[i] = bemenetek[i].LockBits(r, ImageLockMode.ReadOnly, PixelFormat.Format24bppRgb);
                    bemenetPointers[i] = (byte*)bemenetDatas[i].Scan0;
                }

                int rOsszeg = 0, gOsszeg = 0, bOsszeg = 0;

                for (int y = 0; y < H; y++)
                {
                    for (int x = 0; x < W; x++)
                    {
                        //Vesszük minden kép adott pixeljét, és átlagoljuk:
                        rOsszeg = 0; gOsszeg = 0; bOsszeg = 0;
                        for (int i = 0; i < bemenetek.Count; i++)
                        {
                            rOsszeg += bemenetPointers[i][0];
                            gOsszeg += bemenetPointers[i][1];
                            bOsszeg += bemenetPointers[i][2];
                        }
                        p[0] = (byte)((double)rOsszeg / bemenetek.Count);
                        p[1] = (byte)((double)gOsszeg / bemenetek.Count);
                        p[2] = (byte)((double)bOsszeg / bemenetek.Count);


                        //Pointer léptetések:
                        p += 3;
                        for (int i = 0; i < bemenetPointers.Length; i++)
                            bemenetPointers[i] += 3;
                    }
                    p += offset;
                    for (int i = 0; i < bemenetPointers.Length; i++)
                        bemenetPointers[i] += offset;
                }


                for (int i = 0; i < bemenetDatas.Length; i++)
                    bemenetek[i].UnlockBits(bemenetDatas[i]);
                imageOut.UnlockBits(bmData);
                return imageOut;
            }

            public static unsafe Bitmap ADD(Bitmap b1, Bitmap b2)
            {
                Bitmap imageOut = new Bitmap(b1);
                int W = imageOut.Width;
                int H = imageOut.Height;
                Rectangle r = new Rectangle(0, 0, W, H);

                BitmapData imageOutData = imageOut.LockBits(r, ImageLockMode.ReadWrite, PixelFormat.Format24bppRgb);
                int stride = imageOutData.Stride, offset = stride - W * 3;
                byte* p1 = (byte*)imageOutData.Scan0;

                BitmapData b2Data = b2.LockBits(r, ImageLockMode.ReadOnly, PixelFormat.Format24bppRgb);
                byte* p2 = (byte*)b2Data.Scan0;

                int sum = 0;

                for (int y = 0; y < H; y++)
                {
                    for (int x = 0; x < W; x++)
                    {
                        sum = p1[0] + p2[0];
                        p1[0] = sum > 255 ? (byte)255 : (byte)sum;
                        sum = p1[1] + p2[1];
                        p1[1] = sum > 255 ? (byte)255 : (byte)sum;
                        sum = p1[2] + p2[2];
                        p1[2] = sum > 255 ? (byte)255 : (byte)sum;

                        p1 += 3;
                        p2 += 3;
                    }
                    p1 += offset;
                    p2 += offset;
                }

                b2.UnlockBits(b2Data);
                imageOut.UnlockBits(imageOutData);
                return imageOut;
            }
            public static unsafe Bitmap SUB(Bitmap b1, Bitmap b2)
            {
                Bitmap imageOut = new Bitmap(b1);
                int W = imageOut.Width;
                int H = imageOut.Height;
                Rectangle r = new Rectangle(0, 0, W, H);

                BitmapData imageOutData = imageOut.LockBits(r, ImageLockMode.ReadWrite, PixelFormat.Format24bppRgb);
                int stride = imageOutData.Stride, offset = stride - W * 3;
                byte* p1 = (byte*)imageOutData.Scan0;

                BitmapData b2Data = b2.LockBits(r, ImageLockMode.ReadOnly, PixelFormat.Format24bppRgb);
                byte* p2 = (byte*)b2Data.Scan0;

                int result = 0;

                for (int y = 0; y < H; y++)
                {
                    for (int x = 0; x < W; x++)
                    {
                        result = p1[0] - p2[0];
                        p1[0] = result < 0 ? (byte)0 : (byte)result;
                        result = p1[1] - p2[1];
                        p1[1] = result < 0 ? (byte)0 : (byte)result;
                        result = p1[2] - p2[2];
                        p1[2] = result < 0 ? (byte)0 : (byte)result;

                        p1 += 3;
                        p2 += 3;
                    }
                    p1 += offset;
                    p2 += offset;
                }

                b2.UnlockBits(b2Data);
                imageOut.UnlockBits(imageOutData);
                return imageOut;
            }
        }

        public static class GeometriaiTranszformaciok
        {
            public static Bitmap Eltolas(Bitmap imageIn, int vizErtek, int fugErtek)
            {
                return VizszintesEltolas(FuggolegesEltolas(imageIn, fugErtek), vizErtek);
            }
            private static unsafe Bitmap FuggolegesEltolas(Bitmap imageIn, int fugErtek)
            {
                fugErtek = -fugErtek;
                Bitmap imageOut = new Bitmap(imageIn.Width, imageIn.Height, PixelFormat.Format24bppRgb);
                int W = imageOut.Width;
                int H = imageOut.Height;
                Rectangle r = new Rectangle(0, 0, W, H);

                BitmapData imageInData = imageIn.LockBits(r, ImageLockMode.ReadOnly, PixelFormat.Format24bppRgb);
                byte* pIn = (byte*)imageInData.Scan0;

                BitmapData imageOutData = imageOut.LockBits(r, ImageLockMode.ReadWrite, PixelFormat.Format24bppRgb);
                byte* pOut = (byte*)imageOutData.Scan0;

                int stride = imageOutData.Stride, offset = stride - W * 3;

                for (int y = 0; y < H; y++)
                {
                    for (int x = 0; x < W; x++)
                    {
                        if (y + fugErtek < H && y + fugErtek >= 0)
                        {
                            pOut += fugErtek * stride;
                            //elugrunk oda pOut-tal ahova kell írni
                            pOut[0] = pIn[0];
                            pOut[1] = pIn[1];
                            pOut[2] = pIn[2];
                            //visszaugrunk oda pOut-tal, ahol tartottunk
                            pOut -= fugErtek * stride;
                        }

                        pOut += 3;
                        pIn += 3;
                    }
                    pOut += offset;
                    pIn += offset;
                }

                imageIn.UnlockBits(imageInData);
                imageOut.UnlockBits(imageOutData);
                return imageOut;
            }
            private static unsafe Bitmap VizszintesEltolas(Bitmap imageIn, int vizErtek)
            {
                int vizErtekBajtos = 3 * vizErtek;
                Bitmap imageOut = new Bitmap(imageIn.Width, imageIn.Height, PixelFormat.Format24bppRgb);
                int W = imageOut.Width;
                int H = imageOut.Height;
                Rectangle r = new Rectangle(0, 0, W, H);

                BitmapData imageInData = imageIn.LockBits(r, ImageLockMode.ReadOnly, PixelFormat.Format24bppRgb);
                byte* pIn = (byte*)imageInData.Scan0;

                BitmapData imageOutData = imageOut.LockBits(r, ImageLockMode.ReadWrite, PixelFormat.Format24bppRgb);
                byte* pOut = (byte*)imageOutData.Scan0;

                int stride = imageOutData.Stride, offset = stride - W * 3;

                for (int y = 0; y < H; y++)
                {
                    for (int x = 0; x < W; x++)
                    {
                        if (x + vizErtek < W && x + vizErtek >= 0) //ha szélességben (jobbra vagy balra) kimennénk, akkor ott ne csináljuk meg
                        {
                            pOut += vizErtekBajtos;
                            //elugrunk oda pOut-tal ahova kell írni
                            pOut[0] = pIn[0];
                            pOut[1] = pIn[1];
                            pOut[2] = pIn[2];
                            //visszaugrunk oda pOut-tal, ahol tartottunk
                            pOut -= vizErtekBajtos;
                        }

                        pOut += 3;
                        pIn += 3;
                    }
                    pOut += offset;
                    pIn += offset;
                }

                imageIn.UnlockBits(imageInData);
                imageOut.UnlockBits(imageOutData);
                return imageOut;
            }

            public static Bitmap Atmeretezes(Image imageIn, int szelesseg, int magassag)
            {   //http://stackoverflow.com/a/24199315
                var destRect = new Rectangle(0, 0, szelesseg, magassag);
                var destImage = new Bitmap(szelesseg, magassag);

                destImage.SetResolution(imageIn.HorizontalResolution, imageIn.VerticalResolution);

                using (var graphics = Graphics.FromImage(destImage))
                {
                    graphics.CompositingMode = CompositingMode.SourceCopy;
                    graphics.CompositingQuality = CompositingQuality.HighQuality;
                    graphics.InterpolationMode = InterpolationMode.HighQualityBicubic;
                    graphics.SmoothingMode = SmoothingMode.HighQuality;
                    graphics.PixelOffsetMode = PixelOffsetMode.HighQuality;

                    using (var wrapMode = new ImageAttributes())
                    {
                        wrapMode.SetWrapMode(WrapMode.TileFlipXY);
                        graphics.DrawImage(imageIn, destRect, 0, 0, imageIn.Width, imageIn.Height, GraphicsUnit.Pixel, wrapMode);
                    }
                }

                return destImage;
            }

            public static Bitmap Forgatas(Bitmap bmp, float szog)
            {   //http://stackoverflow.com/a/14184934 átírogatva
                Color bkColor = Color.Transparent;
                //szog = -szog;
                if (szog > 180)
                {
                    szog += -360; //ez volt hibás az eredetibe
                }

                System.Drawing.Imaging.PixelFormat pf = default(System.Drawing.Imaging.PixelFormat);
                if (bkColor == Color.Transparent)
                {
                    pf = System.Drawing.Imaging.PixelFormat.Format32bppArgb;
                }
                else
                {
                    pf = bmp.PixelFormat;
                }

                float sin = (float)Math.Abs(Math.Sin(szog * Math.PI / 180.0)); // this function takes radians
                float cos = (float)Math.Abs(Math.Cos(szog * Math.PI / 180.0)); // this one too
                float newImgWidth = sin * bmp.Height + cos * bmp.Width;
                float newImgHeight = sin * bmp.Width + cos * bmp.Height;
                float originX = 0f;
                float originY = 0f;

                if (szog > 0)
                {
                    if (szog <= 90)
                        originX = sin * bmp.Height;
                    else
                    {
                        originX = newImgWidth;
                        originY = newImgHeight - sin * bmp.Width;
                    }
                }
                else
                {
                    if (szog >= -90)
                        originY = sin * bmp.Width;
                    else
                    {
                        originX = newImgWidth - sin * bmp.Height;
                        originY = newImgHeight;
                    }
                }

                Bitmap newImg = new Bitmap((int)newImgWidth, (int)newImgHeight, pf);
                Graphics g = Graphics.FromImage(newImg);
                g.Clear(bkColor);
                g.TranslateTransform(originX, originY); // offset the origin to our calculated values
                g.RotateTransform(szog); // set up rotate
                g.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBilinear;
                g.DrawImageUnscaled(bmp, 0, 0); // draw the image at 0, 0
                g.Dispose();

                return newImg;
            }
        }

        public static class Szurok //Nem félreérteni: nem szurok, hanem szűrők :D
        {
            //Átlagoló
            public static unsafe Bitmap Atlagolo(Bitmap imageIn, int MaszkMeret)
            {
                //ha túl nagy maszkot ad meg, az nincs ellenőrizve

                Bitmap imageOut = new Bitmap(imageIn);
                int W = imageOut.Width;
                int H = imageOut.Height;
                BitmapData imageInData = imageIn.LockBits(new Rectangle(0, 0, W, H), ImageLockMode.ReadOnly, PixelFormat.Format24bppRgb);
                int stride = imageInData.Stride;
                int offset = stride - W * 3;
                byte* pIn = (byte*)imageInData.Scan0;

                BitmapData imageOutData = imageOut.LockBits(new Rectangle(0, 0, W, H), ImageLockMode.WriteOnly, PixelFormat.Format24bppRgb);
                byte* pOut = (byte*)imageOutData.Scan0;

                //pointereket elléptetjük a megfelelő első elemhez: (MaszkMeret / 2). sor (MaszkMeret / 2). pixele  (0-tól indexelve)
                int maszkfele = MaszkMeret >> 1;
                pIn += maszkfele * stride + maszkfele * 3;
                pOut += maszkfele * stride + maszkfele * 3;
                int maszkNegyzet = MaszkMeret * MaszkMeret;

                for (int y = 0; y < H - (maszkfele << 1); y++) //2*maszkfele-szer kell végrehajtani, DE: MaszkMeret != ((MaszkMeret >> 1)<<1) !!!
                {
                    for (int x = 0; x < W - (maszkfele << 1); x++)
                    {
                        //Vegyük a megfelelő ablakot:
                        byte[] ablak = new byte[maszkNegyzet];
                        int cnt = 0;
                        for (int i = -maszkfele; i <= maszkfele; i++)
                        {
                            for (int j = -maszkfele; j <= maszkfele; j++)
                            {
                                ablak[cnt++] = pIn[i * stride + j];
                            }
                        }
                        //Vegyük az összegét:
                        double sum = 0;
                        for (int i = 0; i < ablak.Length; i++)
                            sum += ablak[i];

                        double atlag = sum / maszkNegyzet;

                        pOut[0] = (byte)atlag;
                        pOut[1] = (byte)atlag;
                        pOut[2] = (byte)atlag;

                        pIn += 3;
                        pOut += 3;
                    }
                    pIn += (3 * MaszkMeret - 3) + offset;  //y = f(x) = 3x-3 függvény írja le azt, amit ide kell írni
                    pOut += (3 * MaszkMeret - 3) + offset;
                }

                imageIn.UnlockBits(imageInData);
                imageOut.UnlockBits(imageOutData);
                return imageOut;
            }
            private static unsafe Bitmap Atlagolo3(Bitmap imageIn, int semmi_csak_a_paraméterszám_miatt_kell)
            {
                Bitmap imageOut = new Bitmap(imageIn);
                int W = imageOut.Width;
                int H = imageOut.Height;
                BitmapData imageInData = imageIn.LockBits(new Rectangle(0, 0, W, H), ImageLockMode.ReadOnly, PixelFormat.Format24bppRgb);
                int stride = imageInData.Stride;

                int offset = stride - W * 3;
                byte* pIn = (byte*)imageInData.Scan0;

                BitmapData imageOutData = imageOut.LockBits(new Rectangle(0, 0, W, H), ImageLockMode.WriteOnly, PixelFormat.Format24bppRgb);
                byte* pOut = (byte*)imageOutData.Scan0;

                //pointereket elléptetjük a második sor második pixeléhez:
                pIn += stride + 3;
                pOut += stride + 3;

                for (int y = 0; y < H - 2; y++)
                {
                    for (int x = 0; x < W - 2; x++)
                    {
                        //Minden pixelre meghatározzuk az ablak összegét, és osztjuk 9-cel
                        byte balra = pIn[-1];
                        byte kozep = pIn[0];
                        byte jobbra = pIn[1];
                        byte felette = pIn[-stride];
                        byte alatta = pIn[stride];
                        byte balraFel = pIn[-stride - 1];
                        byte jobbraFel = pIn[-stride + 1];
                        byte balraLe = pIn[stride - 1];
                        byte jobbraLe = pIn[stride + 1];

                        double atlag = (balra + kozep + jobbra + felette + alatta + balraFel + jobbraFel + balraLe + jobbraLe) / 9;

                        pOut[0] = (byte)atlag;
                        pOut[1] = (byte)atlag;
                        pOut[2] = (byte)atlag;

                        pIn += 3;
                        pOut += 3;
                    }
                    pIn += 6 + offset;
                    pOut += 6 + offset;
                }

                imageIn.UnlockBits(imageInData);
                imageOut.UnlockBits(imageOutData);
                return imageOut;
            }

            //Medián
            public static unsafe Bitmap Median(Bitmap imageIn, int MaszkMeret)
            {
                //ha túl nagy maszkot ad meg, az nincs ellenőrizve

                Bitmap imageOut = new Bitmap(imageIn);
                int W = imageOut.Width;
                int H = imageOut.Height;
                BitmapData imageInData = imageIn.LockBits(new Rectangle(0, 0, W, H), ImageLockMode.ReadOnly, PixelFormat.Format24bppRgb);
                int stride = imageInData.Stride;

                int offset = stride - W * 3;
                byte* pIn = (byte*)imageInData.Scan0;

                BitmapData imageOutData = imageOut.LockBits(new Rectangle(0, 0, W, H), ImageLockMode.WriteOnly, PixelFormat.Format24bppRgb);
                byte* pOut = (byte*)imageOutData.Scan0;

                //pointereket elléptetjük a megfelelő első elemhez: (MaszkMeret / 2). sor (MaszkMeret / 2). pixele  (0-tól indexelve)
                int maszkfele = MaszkMeret >> 1;
                pIn += maszkfele * stride + maszkfele * 3;
                pOut += maszkfele * stride + maszkfele * 3;
                int maszkNegyzet = MaszkMeret * MaszkMeret;

                for (int y = 0; y < H - (maszkfele << 1); y++) //2*maszkfele-szer kell végrehajtani, DE: MaszkMeret != ((MaszkMeret >> 1)<<1) !!!
                {
                    for (int x = 0; x < W - (maszkfele << 1); x++)
                    {
                        //Vegyük a megfelelő ablakot:
                        int[] ablak = new int[maszkNegyzet]; //int, mert a MedianCalculator int-re van megírva és nem akartam belenyúlni
                        int cnt = 0;
                        for (int i = -maszkfele; i <= maszkfele; i++)
                        {
                            for (int j = -maszkfele; j <= maszkfele; j++)
                            {
                                ablak[cnt++] = pIn[i * stride + j];
                            }
                        }
                        //Vegyük az ablak mediánját
                        double ujertek = MedianCalculator(ablak, ablak.Length);

                        pOut[0] = (byte)ujertek;
                        pOut[1] = (byte)ujertek;
                        pOut[2] = (byte)ujertek;

                        pIn += 3;
                        pOut += 3;
                    }
                    pIn += (3 * MaszkMeret - 3) + offset;  //y = f(x) = 3x-3 függvény írja le azt, amit ide kell írni
                    pOut += (3 * MaszkMeret - 3) + offset;
                }

                imageIn.UnlockBits(imageInData);
                imageOut.UnlockBits(imageOutData);
                return imageOut;
            }

            //http://stackoverflow.com/a/31672367/4215389   innen másolva (ha már megírták egyszer O(n)-re, akkor minek írjam meg O(n*log(n))-re)
            [System.Runtime.CompilerServices.MethodImpl(System.Runtime.CompilerServices.MethodImplOptions.AggressiveInlining)]
            private static unsafe void SwapElements(int* p, int* q)
            {
                int temp = *p;
                *p = *q;
                *q = temp;
            }
            private static unsafe int MedianCalculator(int[] arr, int n)
            {
                int middle, ll, hh;

                int low = 0; int high = n - 1; int median = (low + high) / 2;
                fixed (int* arrptr = arr)
                {
                    for (; ; )
                    {
                        if (high <= low)
                            return arr[median];

                        if (high == low + 1)
                        {
                            if (arr[low] > arr[high])
                                SwapElements(arrptr + low, arrptr + high);
                            return arr[median];
                        }

                        middle = (low + high) / 2;
                        if (arr[middle] > arr[high])
                            SwapElements(arrptr + middle, arrptr + high);

                        if (arr[low] > arr[high])
                            SwapElements(arrptr + low, arrptr + high);

                        if (arr[middle] > arr[low])
                            SwapElements(arrptr + middle, arrptr + low);

                        SwapElements(arrptr + middle, arrptr + low + 1);

                        ll = low + 1;
                        hh = high;
                        for (; ; )
                        {
                            do ll++; while (arr[low] > arr[ll]);
                            do hh--; while (arr[hh] > arr[low]);

                            if (hh < ll)
                                break;

                            SwapElements(arrptr + ll, arrptr + hh);
                        }

                        SwapElements(arrptr + low, arrptr + hh);

                        if (hh <= median)
                            low = ll;
                        if (hh >= median)
                            high = hh - 1;
                    }
                }
            }

            //Gauss
            public static unsafe Bitmap Gauss(Bitmap imageIn, int MaszkMeret, double sigma)
            {
                //ha túl nagy maszkot ad meg, az nincs ellenőrizve

                //Gauss-maszk kigenerálása:
                int maszkfele = MaszkMeret >> 1;
                double[,] gaussMask = new double[MaszkMeret, MaszkMeret];
                for (int i = -maszkfele; i <= maszkfele; i++)
                {
                    for (int j = -maszkfele; j <= maszkfele; j++)
                    {
                        //http://homepages.inf.ed.ac.uk/rbf/HIPR2/gsmooth.htm
                        gaussMask[i + maszkfele, j + maszkfele] = (1 / (2 * Math.PI * Math.Pow(sigma, 2))) * Math.Exp(-(((Math.Pow(i, 2) + Math.Pow(j, 2))) / (2 * Math.Pow(sigma, 2))));
                    }
                }

                Bitmap imageOut = new Bitmap(imageIn);
                int W = imageOut.Width;
                int H = imageOut.Height;
                BitmapData imageInData = imageIn.LockBits(new Rectangle(0, 0, W, H), ImageLockMode.ReadOnly, PixelFormat.Format24bppRgb);
                int stride = imageInData.Stride;

                int offset = stride - W * 3;
                byte* pIn = (byte*)imageInData.Scan0;

                BitmapData imageOutData = imageOut.LockBits(new Rectangle(0, 0, W, H), ImageLockMode.WriteOnly, PixelFormat.Format24bppRgb);
                byte* pOut = (byte*)imageOutData.Scan0;

                //pointereket elléptetjük a megfelelő első elemhez: (MaszkMeret / 2). sor (MaszkMeret / 2). pixele  (0-tól indexelve)
                pIn += maszkfele * stride + maszkfele * 3;
                pOut += maszkfele * stride + maszkfele * 3;
                int maszkNegyzet = MaszkMeret * MaszkMeret;

                for (int y = 0; y < H - (maszkfele << 1); y++) //2*maszkfele-szer kell végrehajtani, DE: MaszkMeret != ((MaszkMeret >> 1)<<1) !!!
                {
                    for (int x = 0; x < W - (maszkfele << 1); x++)
                    {
                        //Vegyük a megfelelő ablakot:
                        double[] ablak = new double[maszkNegyzet];
                        int cnt = 0;
                        for (int i = -maszkfele; i <= maszkfele; i++)
                        {
                            for (int j = -maszkfele; j <= maszkfele; j++)
                            {
                                ablak[cnt++] = pIn[i * stride + j];
                            }
                        }
                        //Szorozzuk meg a megfelelő elemeket a maszk megfelelő elemeivel:
                        int indexer = 0;
                        for (int i = 0; i < MaszkMeret; i++)
                        {
                            for (int j = 0; j < MaszkMeret; j++)
                            {
                                ablak[indexer] = ablak[indexer++] * gaussMask[i, j];
                            }
                        }
                        double sum = ablak.Sum();

                        pOut[0] = (byte)sum;
                        pOut[1] = (byte)sum;
                        pOut[2] = (byte)sum;

                        pIn += 3;
                        pOut += 3;
                    }
                    pIn += (3 * MaszkMeret - 3) + offset;  //y = f(x) = 3x-3 függvény írja le azt, amit ide kell írni
                    pOut += (3 * MaszkMeret - 3) + offset;
                }

                imageIn.UnlockBits(imageInData);
                imageOut.UnlockBits(imageOutData);
                return imageOut;
            }

            //Élesítés - Laplace
            public static unsafe Bitmap ElesitesLaplace(Bitmap imageIn)
            {//ha túl nagy maszkot ad meg, az nincs ellenőrizve
                int MaszkMeret = 3;
                int maszkfele = MaszkMeret >> 1;
                double[,] maszk = new double[3, 3] {  { 0,  1, 0 },
                                                      { 1, -4, 1 },
                                                      { 0,  1, 0 }};

                Bitmap imageOut = new Bitmap(imageIn);
                int W = imageOut.Width;
                int H = imageOut.Height;
                BitmapData imageInData = imageIn.LockBits(new Rectangle(0, 0, W, H), ImageLockMode.ReadOnly, PixelFormat.Format24bppRgb);
                int stride = imageInData.Stride;
                int offset = stride - imageIn.Width * 3;
                byte* pIn = (byte*)imageInData.Scan0;

                BitmapData imageOutData = imageOut.LockBits(new Rectangle(0, 0, W, H), ImageLockMode.WriteOnly, PixelFormat.Format24bppRgb);
                byte* pOut = (byte*)imageOutData.Scan0;

                //pointereket elléptetjük a megfelelő első elemhez: (MaszkMeret / 2). sor (MaszkMeret / 2). pixele  (0-tól indexelve)
                pIn += maszkfele * stride + maszkfele * 3;
                pOut += maszkfele * stride + maszkfele * 3;
                int maszkNegyzet = MaszkMeret * MaszkMeret;

                for (int y = 0; y < H - (maszkfele << 1); y++) //2*maszkfele-szer kell végrehajtani, DE: MaszkMeret != ((MaszkMeret >> 1)<<1) !!!
                {
                    for (int x = 0; x < W - (maszkfele << 1); x++)
                    {
                        //Vegyük a megfelelő ablakot:
                        double[] ablak = new double[maszkNegyzet];
                        int indexer = 0;
                        for (int i = -maszkfele; i <= maszkfele; i++)
                        {
                            for (int j = -maszkfele; j <= maszkfele; j++)
                            {
                                ablak[indexer++] = pIn[i * stride + j];
                            }
                        }
                        //Szorozzuk meg a megfelelő elemeket a maszk megfelelő elemeivel:
                        indexer = 0;
                        for (int i = 0; i < MaszkMeret; i++)
                        {
                            for (int j = 0; j < MaszkMeret; j++)
                            {
                                ablak[indexer] = ablak[indexer++] * maszk[i, j];
                            }
                        }
                        double sum = ablak.Sum();
                        sum = sum < 0 ? 0 : sum;
                        sum = sum > 255 ? 255 : sum;

                        pOut[0] = (byte)sum;
                        pOut[1] = (byte)sum;
                        pOut[2] = (byte)sum;

                        pIn += 3;
                        pOut += 3;
                    }
                    pIn += (3 * MaszkMeret - 3) + offset;  //f(x) = 3x - 3 függvény írja le azt, amit ide kell írni
                    pOut += (3 * MaszkMeret - 3) + offset;
                }

                imageIn.UnlockBits(imageInData);
                imageOut.UnlockBits(imageOutData);

                return EgeszKepesMuveletek.SUB(imageIn, imageOut);
            }

            public static unsafe Bitmap HarrisSarokDetektor(Bitmap imageIn, int Threshold)
            {
                int[, ,] gradiensek = new int[imageIn.Height, imageIn.Width * 3, 3];

                Bitmap imageOut = new Bitmap(imageIn);
                int W = imageOut.Width;
                int H = imageOut.Height;
                BitmapData imageInData = imageIn.LockBits(new Rectangle(0, 0, W, H), ImageLockMode.ReadOnly, PixelFormat.Format24bppRgb);
                BitmapData imageOutData = imageOut.LockBits(new Rectangle(0, 0, W, H), ImageLockMode.WriteOnly, PixelFormat.Format24bppRgb);
                int stride = imageInData.Stride;

                int offset = stride - W * 3;

                byte* pIn = (byte*)imageInData.Scan0;
                byte* pOut = (byte*)imageOutData.Scan0;

                //pointereket elléptetjük a megfelelő első elemhez
                pIn += stride + 3;
                pOut += stride + 3;

                for (int y = 0; y < H - 2; y++)
                {
                    for (int x = 0; x < W - 2; x++)
                    {
                        //Vegyük a megfelelő ablakot (minek for ciklus ha kevés elem van?):
                        byte balra = pIn[-1];
                        byte kozep = pIn[0];
                        byte jobbra = pIn[1];
                        byte felette = pIn[-stride];
                        byte alatta = pIn[stride];
                        byte balraFel = pIn[-stride - 1];
                        byte jobbraFel = pIn[-stride + 1];
                        byte balraLe = pIn[stride - 1];
                        byte jobbraLe = pIn[stride + 1];

                        int gradX = jobbraFel - balraFel + jobbra - balra + jobbraLe - balraLe;
                        int gradY = balraLe - balraFel + alatta - felette + jobbraLe - jobbraFel;

                        gradiensek[y + 1, x + 1, 0] = gradX * gradX;
                        gradiensek[y + 1, x + 1, 1] = gradY * gradY;
                        gradiensek[y + 1, x + 1, 2] = gradX * gradY;

                        pOut[0] = (byte)0;
                        pOut[1] = (byte)0;
                        pOut[2] = (byte)0;

                        pIn += 3;
                        pOut += 3;
                    }
                    pIn += 6 + offset;
                    pOut += 6 + offset;
                }

                pOut = (byte*)imageOutData.Scan0;
                pOut += stride + 3;

                for (int y = 0; y < H - 2; y++)
                {
                    for (int x = 0; x < W - 2; x++)
                    {
                        double sumGradX = 0;
                        double sumGradY = 0;
                        double sumGradXY = 0;
                        for (int i = -1; i <= 1; i++)
                        {
                            for (int j = -1; j <= 1; j++)
                            {
                                sumGradX    += gradiensek[y + 1 + i, x + 1 + j, 0];
                                sumGradY    += gradiensek[y + 1 + i, x + 1 + j, 1];
                                sumGradXY   += gradiensek[y + 1 + i, x + 1 + j, 2];
                            }
                        }

                        //det(A) / Nyom(A)
                        double kiszamolt = (sumGradX * sumGradY - sumGradXY * sumGradXY) / (sumGradX + sumGradY);

                        if (kiszamolt > Threshold)
                        {
                            pOut[0] = (byte)255;
                            pOut[1] = (byte)255;
                            pOut[2] = (byte)255;
                        }

                        pOut += 3;
                    }
                    pOut += 6 + offset;
                }


                imageIn.UnlockBits(imageInData);
                imageOut.UnlockBits(imageOutData);
                return imageOut;
            }
        }
    }
}