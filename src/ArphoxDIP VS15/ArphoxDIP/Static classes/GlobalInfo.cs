﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing.Imaging;
using System.Drawing;
using ExternalHistogram;

namespace ArphoxDIP
{
    public static class GlobalInfo
    {
        public static string Verzio = "1.0";
        public static string Nevjegy = String.Format("ArphoxDIP v{0}\n\nTulajdonos: Ozsvárt Károly\nE-mail: ozsvart.karoly@gmail.com\n\n2015-16-1. félév", Verzio);


        public static int TalcaMagassag = 40;
        public static double ZoomMertek = 0.1;
        public static int TesztekSzama = 5;
    }
}