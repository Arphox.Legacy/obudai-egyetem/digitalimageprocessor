﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Windows;
using System.Linq;

namespace ArphoxDIP
{
    public partial class MainWindow : Window            // MENU CLICKS
    {
        #region Fájl

        private void MenuClick_KepBetoltes(object sender, RoutedEventArgs e)
        {
            string path = KepBetoltoAblak();
            if (path != null)
            {
                KepBetoltes(path);
            }
        }
        private string KepBetoltoAblak()
        {
            OpenFileDialog ablak = new OpenFileDialog();
            ablak.Filter = "Kép (*.jpg, *.jpeg, *.png, *.bmp, *.gif, *.tif) | *.jpg; *.jpeg; *.png; *.bmp; *.gif; *.tif";

            if (Directory.Exists(@"e:\Ozsvárt Károly\OE\ Tantárgyak\ROBOTIKA\Szenzorok és robotlátás I\ArphoxDIP VS15\_images\"))
                ablak.InitialDirectory = @"e:\Ozsvárt Károly\OE\ Tantárgyak\ROBOTIKA\Szenzorok és robotlátás I\ArphoxDIP VS15\_images\";
            else
                ablak.InitialDirectory = System.Environment.CurrentDirectory;

            if (ablak.ShowDialog() == true)
            {
                return ablak.FileName;
            }
            else
            {
                return null;
            }
        }
        private void MenuClick_HisztogramBetoltes(object sender, RoutedEventArgs e)
        {
            OpenFileDialog ablak = new OpenFileDialog();
            ablak.Filter = "Hisztogram (*.txt) | *.txt";
            ablak.Multiselect = true;

            if (Directory.Exists(@"e:\Ozsvárt Károly\OE\ Tantárgyak\ROBOTIKA\Szenzorok és robotlátás I\ArphoxDIP\_histograms\"))
                ablak.InitialDirectory = @"e:\Ozsvárt Károly\OE\ Tantárgyak\ROBOTIKA\Szenzorok és robotlátás I\ArphoxDIP\_histograms\";
            else
                ablak.InitialDirectory = System.Environment.CurrentDirectory;

            if (ablak.ShowDialog() == true)
            {
                if (ablak.FileNames.Length == 3) //Ha 3 fájlt jelölt ki, RGB hisztogram
                {   //Töltsük be a path-okba a megadott fájlokat
                    string[] paths = new string[3];
                    for (int i = 0; i < 3; i++)
                    {
                        paths[i] = ablak.FileNames[i];
                    }
                    Array.Sort(paths); //Így B,G,R lesz a sorrend
                    HisztogramBetoltes_RGB(paths);
                }
                //Egy fájlt jelölt ki:
                else if (ablak.FileName.ToLower().EndsWith(".txt"))
                {
                    HisztogramBetoltes_Gray(ablak.FileName);
                }
            }
        }

        private void MenuClick_Mentes(object sender, RoutedEventArgs e)
        {
            if (info.LoadedObjectType == ObjectType.Nothing)
                return;

            #region Képmentés
            if (info.LoadedObjectType == ObjectType.Image)
            {
                SaveFileDialog ablak = new SaveFileDialog();
                ablak.ValidateNames = true;

                if (Directory.Exists(@"e:\Ozsvárt Károly\OE\ Tantárgyak\ROBOTIKA\Szenzorok és robotlátás I\ArphoxDIP\_images\"))
                    ablak.InitialDirectory = @"e:\Ozsvárt Károly\OE\ Tantárgyak\ROBOTIKA\Szenzorok és robotlátás I\ArphoxDIP\_images\";
                else
                    ablak.InitialDirectory = System.Environment.CurrentDirectory;

                if (ablak.ShowDialog() == true)
                {
                    string fileName = System.IO.Path.GetFullPath(ablak.FileName);
                    string fileExt = System.IO.Path.GetExtension(ablak.FileName);

                    //ha nem írt kiterjesztést, akkor odaírjuk neki
                    if (fileExt == string.Empty)
                    {
                        fileName += info.LoadedImageExtension;
                    }

                    Kep.Save(fileName);
                }
                return;
            }
            #endregion

            #region Hisztogram mentés
            if (info.LoadedHistogramType == HistogramType.Grayscale)
            {
                SaveFileDialog ablak = new SaveFileDialog();
                ablak.ValidateNames = true;

                if (Directory.Exists(@"e:\Ozsvárt Károly\OE\ Tantárgyak\ROBOTIKA\Szenzorok és robotlátás I\ArphoxDIP\_histograms\"))
                    ablak.InitialDirectory = @"e:\Ozsvárt Károly\OE\ Tantárgyak\ROBOTIKA\Szenzorok és robotlátás I\ArphoxDIP\_histograms\";
                else
                    ablak.InitialDirectory = System.Environment.CurrentDirectory;

                if (ablak.ShowDialog() == true)
                {
                    string pathAndFilename = System.IO.Path.GetFullPath(ablak.FileName);
                    string fileExtension = System.IO.Path.GetExtension(ablak.FileName);
                    if (fileExtension != string.Empty) //Ha beírt valami kiterjesztést, akkor azt le kell szedni a Path-ból
                        pathAndFilename = pathAndFilename.Replace(fileExtension, ""); //Mivel a GetFullPath kiterjesztéssel adja, ezért azt le kell csípni

                    fileExtension = ".txt"; //mindenképpen .txt-be akarjuk menteni

                    string path = pathAndFilename + fileExtension;
                    FileOperations.SaveHistogram(((GrayscaleHistogram)hisztogram).Data, path);
                }
            }
            else //HistogramType.RGB
            {
                SaveFileDialog ablak = new SaveFileDialog();
                ablak.ValidateNames = true;

                if (Directory.Exists(@"e:\Ozsvárt Károly\OE\ Tantárgyak\ROBOTIKA\Szenzorok és robotlátás I\ArphoxDIP\_histograms\"))
                    ablak.InitialDirectory = @"e:\Ozsvárt Károly\OE\ Tantárgyak\ROBOTIKA\Szenzorok és robotlátás I\ArphoxDIP\_histograms\";
                else
                    ablak.InitialDirectory = System.Environment.CurrentDirectory;

                if (ablak.ShowDialog() == true)
                {
                    string pathAndFilename = System.IO.Path.GetFullPath(ablak.FileName);
                    string fileExtension = System.IO.Path.GetExtension(ablak.FileName);
                    if (fileExtension != string.Empty)
                        pathAndFilename = pathAndFilename.Replace(fileExtension, ""); //Mivel a GetFullPath kiterjesztéssel adja, ezért azt le kell csípni

                    fileExtension = ".txt"; //mindenképpen .txt-be akarjuk menteni

                    RGBHistogram h = (RGBHistogram)hisztogram;
                    FileOperations.SaveHistogram(h.R, pathAndFilename + "_R" + fileExtension);
                    FileOperations.SaveHistogram(h.G, pathAndFilename + "_G" + fileExtension);
                    FileOperations.SaveHistogram(h.B, pathAndFilename + "_B" + fileExtension);
                }
            }
            #endregion

        }

        private void MenuClick_Kilepes(object sender, RoutedEventArgs e)
        {
            Application.Current.Shutdown();
        }

        #endregion

        #region Pont alapú műveletek

        private void MenuClick_Pont_Invertalas(object sender, RoutedEventArgs e)
        {
            if (Kep != null)
            {
                if (info.TesztMod)
                {
                    List<long> times = TesztStart();

                    for (int i = 0; i < GlobalInfo.TesztekSzama; i++)
                    {
                        Stopper.Start();
                        ImageProcessor.PontMuveletek.Invertalas(KepMasolat);
                        Stopper.Stop();
                        times.Add(Stopper.ElapsedMilliseconds);
                        Stopper.Reset();
                    }

                    TesztEnd(times);
                }
                else
                {
                    TaskStartImage();
                    paramobj_outgoing.Image = ImageProcessor.PontMuveletek.Invertalas(KepMasolat);
                    TaskEnd();
                }

            }
        }
        private void MenuClick_Pont_Grayscale(object sender, RoutedEventArgs e)
        {
            if (Kep != null)
            {
                if (info.TesztMod)
                {
                    List<long> times = TesztStart();

                    for (int i = 0; i < GlobalInfo.TesztekSzama; i++)
                    {
                        Stopper.Start();
                        ImageProcessor.PontMuveletek.Grayscale(KepMasolat);
                        Stopper.Stop();
                        times.Add(Stopper.ElapsedMilliseconds);
                        Stopper.Reset();
                    }

                    TesztEnd(times);
                }
                else
                {
                    TaskStartImage();
                    paramobj_outgoing.Image = ImageProcessor.PontMuveletek.Grayscale(KepMasolat);
                    TaskEnd();
                }
            }
        }
        private void MenuClick_Pont_IntenzitasSzintreVagas_Normal(object sender, RoutedEventArgs e)
        {
            if (Kep != null)
            {
                Ablakok.TwoDataWindow ablak = new Ablakok.TwoDataWindow("Low: ", "High: ");
                ablak.Data1LowerBound = ablak.Data2LowerBound = 0;
                ablak.Data1UpperBound = ablak.Data2UpperBound = 255;
                ablak.Data1_OnlyInteger = ablak.Data2_OnlyInteger = true;
                ablak.Data1StartValue = 150;
                ablak.Data2StartValue = 200;

                if (ablak.ShowDialog() == true)
                {
                    if (info.TesztMod)
                    {
                        List<long> times = TesztStart();

                        for (int i = 0; i < GlobalInfo.TesztekSzama; i++)
                        {
                            Stopper.Start();
                            ImageProcessor.PontMuveletek.IntenzitasSzintreVagas_Normal(KepMasolat, (byte)ablak.Data1, (byte)ablak.Data2);
                            Stopper.Stop();
                            times.Add(Stopper.ElapsedMilliseconds);
                            Stopper.Reset();
                        }

                        TesztEnd(times);
                    }
                    else
                    {
                        TaskStartImage();
                        paramobj_outgoing.Image = ImageProcessor.PontMuveletek.IntenzitasSzintreVagas_Normal(KepMasolat, (byte)ablak.Data1, (byte)ablak.Data2);
                        TaskEnd();
                    }
                }
            }
        }
        private void MenuClick_Pont_IntenzitasSzintreVagas_DuplaKuszoboles(object sender, RoutedEventArgs e)
        {
            if (Kep != null)
            {
                Ablakok.TwoDataWindow ablak = new Ablakok.TwoDataWindow("Low: ", "High: ");
                ablak.Data1LowerBound = ablak.Data2LowerBound = 0;
                ablak.Data1UpperBound = ablak.Data2UpperBound = 255;
                ablak.Data1_OnlyInteger = ablak.Data2_OnlyInteger = true;
                ablak.Data1StartValue = 150;
                ablak.Data2StartValue = 200;

                if (ablak.ShowDialog() == true)
                {
                    if (info.TesztMod)
                    {
                        List<long> times = TesztStart();

                        for (int i = 0; i < GlobalInfo.TesztekSzama; i++)
                        {
                            Stopper.Start();
                            ImageProcessor.PontMuveletek.IntenzitasSzintreVagas_DuplaKuszoboles(KepMasolat, (byte)ablak.Data1, (byte)ablak.Data2);
                            Stopper.Stop();
                            times.Add(Stopper.ElapsedMilliseconds);
                            Stopper.Reset();
                        }

                        TesztEnd(times);
                    }
                    else
                    {
                        TaskStartImage();
                        paramobj_outgoing.Image = ImageProcessor.PontMuveletek.IntenzitasSzintreVagas_DuplaKuszoboles(KepMasolat, (byte)ablak.Data1, (byte)ablak.Data2);
                        TaskEnd();
                    }
                }
            }
        }
        private void MenuClick_Pont_KontrasztNyujtas_Binaris(object sender, RoutedEventArgs e)   //Más néven küszöbölés
        {
            if (Kep != null)
            {
                Ablakok.OneDataWindow ablak = new Ablakok.OneDataWindow("M: ");
                ablak.Data1LowerBound = 0;
                ablak.Data1UpperBound = 255;
                ablak.Data1_OnlyInteger = true;
                ablak.Data1StartValue = 127;

                if (ablak.ShowDialog() == true)
                {
                    if (info.TesztMod)
                    {
                        List<long> times = TesztStart();

                        for (int i = 0; i < GlobalInfo.TesztekSzama; i++)
                        {
                            Stopper.Start();
                            ImageProcessor.PontMuveletek.KontrasztNyujtasBinaris(KepMasolat, (byte)ablak.Data1);
                            Stopper.Stop();
                            times.Add(Stopper.ElapsedMilliseconds);
                            Stopper.Reset();
                        }

                        TesztEnd(times);
                    }
                    else
                    {
                        TaskStartImage();
                        paramobj_outgoing.Image = ImageProcessor.PontMuveletek.KontrasztNyujtasBinaris(KepMasolat, (byte)ablak.Data1);
                        TaskEnd();
                    }
                }
            }
        }
        private void MenuClick_Pont_KontrasztNyujtas_Skalazo(object sender, RoutedEventArgs e)
        {
            if (Kep != null)
            {
                Ablakok.TwoDataWindow ablak = new Ablakok.TwoDataWindow("M: ", "Skála (1-1000): ");
                ablak.Data1LowerBound = 0;
                ablak.Data1UpperBound = 255;
                ablak.Data1_OnlyInteger = true;
                ablak.Data1StartValue = 127;

                ablak.Data2LowerBound = 1;
                ablak.Data2UpperBound = 1000;
                ablak.Data2_OnlyInteger = true;
                ablak.Data2StartValue = 200;

                if (ablak.ShowDialog() == true)
                {
                    if (info.TesztMod)
                    {
                        List<long> times = TesztStart();

                        for (int i = 0; i < GlobalInfo.TesztekSzama; i++)
                        {
                            Stopper.Start();
                            ImageProcessor.PontMuveletek.KontrasztNyujtasSkalazo(KepMasolat, (byte)ablak.Data1, ablak.Data2);
                            Stopper.Stop();
                            times.Add(Stopper.ElapsedMilliseconds);
                            Stopper.Reset();
                        }

                        TesztEnd(times);
                    }
                    else
                    {
                        TaskStartImage();
                        paramobj_outgoing.Image = ImageProcessor.PontMuveletek.KontrasztNyujtasSkalazo(KepMasolat, (byte)ablak.Data1, ablak.Data2);
                        TaskEnd();
                    }
                }
            }
        }
        private void MenuClick_Pont_KontrasztAllitas(object sender, RoutedEventArgs e)
        {
            if (Kep != null)
            {
                Ablakok.OneDataWindow ablak = new Ablakok.OneDataWindow("-100 -> 100: (%)");
                ablak.Data1LowerBound = -100;
                ablak.Data1UpperBound = 100;
                ablak.Data1_OnlyInteger = true;
                ablak.Data1StartValue = 0;

                if (ablak.ShowDialog() == true)
                {
                    if (info.TesztMod)
                    {
                        List<long> times = TesztStart();

                        for (int i = 0; i < GlobalInfo.TesztekSzama; i++)
                        {
                            Stopper.Start();
                            ImageProcessor.PontMuveletek.KontrasztAllitas(KepMasolat, ablak.Data1);
                            Stopper.Stop();
                            times.Add(Stopper.ElapsedMilliseconds);
                            Stopper.Reset();
                        }

                        TesztEnd(times);
                    }
                    else
                    {
                        TaskStartImage();
                        paramobj_outgoing.Image = ImageProcessor.PontMuveletek.KontrasztAllitas(KepMasolat, ablak.Data1);
                        TaskEnd();
                    }
                }
            }
        }
        private void MenuClick_Pont_LogaritmikusSkalazas(object sender, RoutedEventArgs e)
        {
            if (Kep != null)
            {
                Ablakok.TwoDataWindow ablak = new Ablakok.TwoDataWindow("Log. alap: ", "Konstans szorzó: ");

                ablak.Data1LowerBound = Double.Epsilon;
                ablak.Data1UpperBound = Double.MaxValue;
                ablak.Data1_OnlyInteger = false;
                ablak.Data1StartValue = 2;

                ablak.Data2LowerBound = Double.MinValue;
                ablak.Data2UpperBound = Double.MaxValue;
                ablak.Data2_OnlyInteger = false;
                ablak.Data2StartValue = 20;

                if (ablak.ShowDialog() == true)
                {
                    if (info.TesztMod)
                    {
                        List<long> times = TesztStart();

                        for (int i = 0; i < GlobalInfo.TesztekSzama; i++)
                        {
                            Stopper.Start();
                            ImageProcessor.PontMuveletek.LogaritmikusSkalazas(KepMasolat, ablak.Data1, ablak.Data2);
                            Stopper.Stop();
                            times.Add(Stopper.ElapsedMilliseconds);
                            Stopper.Reset();
                        }

                        TesztEnd(times);
                    }
                    else
                    {
                        TaskStartImage();
                        paramobj_outgoing.Image = ImageProcessor.PontMuveletek.LogaritmikusSkalazas(KepMasolat, ablak.Data1, ablak.Data2);
                        TaskEnd();
                    }
                }
            }
        }
        private void MenuClick_Pont_GammaKorrekcio(object sender, RoutedEventArgs e)
        {
            if (Kep != null)
            {
                Ablakok.OneDataWindow ablak = new Ablakok.OneDataWindow("Mérték: (0.0 => 5.0): ");

                ablak.Data1LowerBound = 0.0;
                ablak.Data1UpperBound = 5.0;
                ablak.Data1_OnlyInteger = false;
                ablak.Data1StartValue = 1.5;

                if (ablak.ShowDialog() == true)
                {
                    if (info.TesztMod)
                    {
                        List<long> times = TesztStart();

                        for (int i = 0; i < GlobalInfo.TesztekSzama; i++)
                        {
                            Stopper.Start();
                            ImageProcessor.PontMuveletek.GammaKorrekcio(KepMasolat, ablak.Data1);
                            Stopper.Stop();
                            times.Add(Stopper.ElapsedMilliseconds);
                            Stopper.Reset();
                        }

                        TesztEnd(times);
                    }
                    else
                    {
                        TaskStartImage();
                        paramobj_outgoing.Image = ImageProcessor.PontMuveletek.GammaKorrekcio(KepMasolat, ablak.Data1);
                        TaskEnd();
                    }
                }
            }
        }
        private void MenuClick_Pont_BitSikonkentiVagas(object sender, RoutedEventArgs e)
        {
            if (Kep != null)
            {
                Ablakok.OneDataWindow ablak = new Ablakok.OneDataWindow("Bit sík (1 -> 8):");
                ablak.Data1LowerBound = 1;
                ablak.Data1UpperBound = 8;
                ablak.Data1_OnlyInteger = true;
                ablak.Data1StartValue = 1;

                if (ablak.ShowDialog() == true)
                {
                    if (info.TesztMod)
                    {
                        List<long> times = TesztStart();

                        for (int i = 0; i < GlobalInfo.TesztekSzama; i++)
                        {
                            Stopper.Start();
                            ImageProcessor.PontMuveletek.BitSikonkentiVagas(KepMasolat, (byte)ablak.Data1);
                            Stopper.Stop();
                            times.Add(Stopper.ElapsedMilliseconds);
                            Stopper.Reset();
                        }

                        TesztEnd(times);
                    }
                    else
                    {
                        TaskStartImage();
                        paramobj_outgoing.Image = ImageProcessor.PontMuveletek.BitSikonkentiVagas(KepMasolat, (byte)ablak.Data1);
                        TaskEnd();
                    }
                }
            }
        }
        private void MenuClick_Pont_Hibadiffuzio(object sender, RoutedEventArgs e)
        {
            if (Kep != null)
            {
                if (info.TesztMod)
                {
                    List<long> times = TesztStart();

                    for (int i = 0; i < GlobalInfo.TesztekSzama; i++)
                    {
                        Stopper.Start();
                        ImageProcessor.PontMuveletek.Hibadiffuzio(KepMasolat);
                        Stopper.Stop();
                        times.Add(Stopper.ElapsedMilliseconds);
                        Stopper.Reset();
                    }

                    TesztEnd(times);
                }
                else
                {
                    TaskStartImage();
                    paramobj_outgoing.Image = ImageProcessor.PontMuveletek.Hibadiffuzio(KepMasolat);
                    TaskEnd();
                }
            }
        }

        #endregion

        #region Hisztogram műveletek

        private void MenuClick_His_NormalHisztogramGeneralas(object sender, RoutedEventArgs e)
        {
            if (Kep != null)
            {
                if (MenuClicks_ImageTypeNotReady())
                    return;

                if (info.TesztMod)
                {
                    List<long> times = TesztStart();

                    for (int i = 0; i < GlobalInfo.TesztekSzama; i++)
                    {
                        Stopper.Start();
                        ImageProcessor.HisztogramMuveletek.NormalHisztogram(Kep, info.LoadedImageType);
                        Stopper.Stop();
                        times.Add(Stopper.ElapsedMilliseconds);
                        Stopper.Reset();
                    }

                    TesztEnd(times);
                }
                else
                {
                    TaskStartHistogram();
                    paramobj_outgoing.Histogram = ImageProcessor.HisztogramMuveletek.NormalHisztogram(Kep, info.LoadedImageType);
                    TaskEnd();
                }
            }
        }
        private void MenuClick_His_HalmozottHisztogramGeneralas(object sender, RoutedEventArgs e)
        {
            if (Kep != null)
            {
                if (MenuClicks_ImageTypeNotReady())
                    return;

                if (info.TesztMod)
                {
                    List<long> times = TesztStart();

                    for (int i = 0; i < GlobalInfo.TesztekSzama; i++)
                    {
                        Stopper.Start();
                        ImageProcessor.HisztogramMuveletek.HalmozottHisztogram(Kep, info.LoadedImageType);
                        Stopper.Stop();
                        times.Add(Stopper.ElapsedMilliseconds);
                        Stopper.Reset();
                    }

                    TesztEnd(times);
                }
                else
                {
                    TaskStartHistogram();
                    paramobj_outgoing.Histogram = ImageProcessor.HisztogramMuveletek.HalmozottHisztogram(Kep, info.LoadedImageType);
                    TaskEnd();
                }
            }
        }
        private void MenuClick_His_NormalizaltHisztogramGeneralas(object sender, RoutedEventArgs e)
        {
            if (Kep != null)
            {
                if (MenuClicks_ImageTypeNotReady())
                    return;

                if (info.TesztMod)
                {
                    List<long> times = TesztStart();

                    for (int i = 0; i < GlobalInfo.TesztekSzama; i++)
                    {
                        Stopper.Start();
                        ImageProcessor.HisztogramMuveletek.NormalizaltHisztogram(Kep, info.LoadedImageType);
                        Stopper.Stop();
                        times.Add(Stopper.ElapsedMilliseconds);
                        Stopper.Reset();
                    }

                    TesztEnd(times);
                }
                else
                {
                    TaskStartHistogram();
                    paramobj_outgoing.Histogram = ImageProcessor.HisztogramMuveletek.NormalizaltHisztogram(Kep, info.LoadedImageType);
                    TaskEnd();
                }
            }
        }
        private void MenuClick_His_NormalizaltHalmozottHisztogramGeneralas(object sender, RoutedEventArgs e)
        {
            if (Kep != null)
            {
                if (MenuClicks_ImageTypeNotReady())
                    return;

                if (info.TesztMod)
                {
                    List<long> times = TesztStart();

                    for (int i = 0; i < GlobalInfo.TesztekSzama; i++)
                    {
                        Stopper.Start();
                        ImageProcessor.HisztogramMuveletek.HalmozottNormalizaltHisztogram(Kep, info.LoadedImageType);
                        Stopper.Stop();
                        times.Add(Stopper.ElapsedMilliseconds);
                        Stopper.Reset();
                    }

                    TesztEnd(times);
                }
                else
                {
                    TaskStartHistogram();
                    paramobj_outgoing.Histogram = ImageProcessor.HisztogramMuveletek.HalmozottNormalizaltHisztogram(Kep, info.LoadedImageType);
                    TaskEnd();
                }
            }
        }

        private bool MenuClicks_ImageTypeNotReady()
        {
            if (info.LoadedImageType == ImageType.Unknown)   //Ha még véletlen futna a Task, ami azt nézni hogy grayscale-e
            {
                MessageBox.Show("Kérlek várj pár másodpercet, amíg végez a kép formátumát (RGB v. szürkeárnyalatos) meghatározó háttérfolyamat!", "Információ", MessageBoxButton.OK, MessageBoxImage.Information);
                return true;
            }
            else
                return false;
        }

        private void MenuClick_His_HisztogramSzethuzas(object sender, RoutedEventArgs e)
        {
            if (Kep != null)
            {
                if (MenuClicks_ImageTypeNotReady())
                    return;

                if (info.TesztMod)
                {
                    List<long> times = TesztStart();

                    for (int i = 0; i < GlobalInfo.TesztekSzama; i++)
                    {
                        Stopper.Start();
                        ImageProcessor.HisztogramMuveletek.HisztogramSzethuzas(KepMasolat);
                        Stopper.Stop();
                        times.Add(Stopper.ElapsedMilliseconds);
                        Stopper.Reset();
                    }

                    TesztEnd(times);
                }
                else
                {
                    TaskStartImage();
                    paramobj_outgoing.Image = ImageProcessor.HisztogramMuveletek.HisztogramSzethuzas(KepMasolat);
                    TaskEnd();
                }
            }
        }
        private void MenuClick_His_KontrasztSzethuzas(object sender, RoutedEventArgs e)
        {
            if (Kep != null)
            {
                if (MenuClicks_ImageTypeNotReady())
                    return;

                Ablakok.TwoDataWindow ablak = new Ablakok.TwoDataWindow("Low: ", "High: ");

                ablak.Data1LowerBound = 0;
                ablak.Data1UpperBound = 255;
                ablak.Data1_OnlyInteger = true;
                ablak.Data1StartValue = 80;

                ablak.Data2LowerBound = 0;
                ablak.Data2UpperBound = 255;
                ablak.Data2_OnlyInteger = true;
                ablak.Data2StartValue = 160;

                if (ablak.ShowDialog() == true)
                {
                    if (info.TesztMod)
                    {
                        List<long> times = TesztStart();

                        for (int i = 0; i < GlobalInfo.TesztekSzama; i++)
                        {
                            Stopper.Start();
                            ImageProcessor.HisztogramMuveletek.KontrasztSzethuzas(KepMasolat, (byte)ablak.Data1, (byte)ablak.Data2);
                            Stopper.Stop();
                            times.Add(Stopper.ElapsedMilliseconds);
                            Stopper.Reset();
                        }

                        TesztEnd(times);
                    }
                    else
                    {
                        TaskStartImage();
                        paramobj_outgoing.Image = ImageProcessor.HisztogramMuveletek.KontrasztSzethuzas(KepMasolat, (byte)ablak.Data1, (byte)ablak.Data2);
                        TaskEnd();
                    }
                }
            }
        }
        private void MenuClick_His_HisztogramKiegyenlites(object sender, RoutedEventArgs e)
        {
            if (Kep != null)
            {
                if (MenuClicks_ImageTypeNotReady())
                    return;

                if (info.TesztMod)
                {
                    List<long> times = TesztStart();

                    for (int i = 0; i < GlobalInfo.TesztekSzama; i++)
                    {
                        Stopper.Start();
                        ImageProcessor.HisztogramMuveletek.HisztogramKiegyenlites(KepMasolat);
                        Stopper.Stop();
                        times.Add(Stopper.ElapsedMilliseconds);
                        Stopper.Reset();
                    }

                    TesztEnd(times);
                }
                else
                {
                    TaskStartImage();
                    paramobj_outgoing.Image = ImageProcessor.HisztogramMuveletek.HisztogramKiegyenlites(KepMasolat);
                    TaskEnd();
                }
            }
        }
        #endregion

        #region Egész képes műveletek
        private void MenuClick_EgeszKep_OR(object sender, RoutedEventArgs e)
        {
            string imagePath = KepBetoltoAblak();
            string maskPath = KepBetoltoAblak();

            if (imagePath == null || maskPath == null)
                return;

            Bitmap original = (Bitmap)Bitmap.FromFile(imagePath);
            info.LoadedImageExtension = System.IO.Path.GetExtension(imagePath);
            Bitmap mask = (Bitmap)Bitmap.FromFile(maskPath);

            if (info.TesztMod)
            {
                List<long> times = TesztStart();

                for (int i = 0; i < GlobalInfo.TesztekSzama; i++)
                {
                    Stopper.Start();
                    ImageProcessor.EgeszKepesMuveletek.OR(original, mask);
                    Stopper.Stop();
                    times.Add(Stopper.ElapsedMilliseconds);
                    Stopper.Reset();
                }

                TesztEnd(times);
            }
            else
            {
                Stopper.Start();
                paramobj_outgoing.Image = ImageProcessor.EgeszKepesMuveletek.OR(original, mask);
                TaskEnd();
            }
        }
        private void MenuClick_EgeszKep_AND(object sender, RoutedEventArgs e)
        {
            string imagePath = KepBetoltoAblak();
            string maskPath = KepBetoltoAblak();

            if (imagePath == null || maskPath == null)
                return;

            Bitmap original = (Bitmap)Bitmap.FromFile(imagePath);
            info.LoadedImageExtension = System.IO.Path.GetExtension(imagePath);
            Bitmap mask = (Bitmap)Bitmap.FromFile(maskPath);

            if (info.TesztMod)
            {
                List<long> times = TesztStart();

                for (int i = 0; i < GlobalInfo.TesztekSzama; i++)
                {
                    Stopper.Start();
                    ImageProcessor.EgeszKepesMuveletek.AND(original, mask);
                    Stopper.Stop();
                    times.Add(Stopper.ElapsedMilliseconds);
                    Stopper.Reset();
                }

                TesztEnd(times);
            }
            else
            {
                Stopper.Start();
                paramobj_outgoing.Image = ImageProcessor.EgeszKepesMuveletek.AND(original, mask);
                TaskEnd();
            }
        }
        private void MenuClick_EgeszKep_AVG(object sender, RoutedEventArgs e)
        {
            OpenFileDialog ablak = new OpenFileDialog();
            ablak.Multiselect = true;
            ablak.Filter = "Kép (*.jpg, *.jpeg, *.png, *.bmp, *.gif, *.tif) | *.jpg; *.jpeg; *.png; *.bmp; *.gif; *.tif";

            if (Directory.Exists(@"e:\Ozsvárt Károly\OE\ Tantárgyak\ROBOTIKA\Szenzorok és robotlátás I\ArphoxDIP\_images\"))
                ablak.InitialDirectory = @"e:\Ozsvárt Károly\OE\ Tantárgyak\ROBOTIKA\Szenzorok és robotlátás I\ArphoxDIP\_images\";
            else
                ablak.InitialDirectory = System.Environment.CurrentDirectory;

            if (ablak.ShowDialog() == true)
            {
                if (info.TesztMod)
                {
                    List<long> times = TesztStart();

                    for (int i = 0; i < GlobalInfo.TesztekSzama; i++)
                    {
                        Stopper.Start();
                        List<Bitmap> bitmaps = new List<Bitmap>();
                        for (int i2 = 0; i2 < ablak.FileNames.Length; i2++)
                        {
                            Bitmap b = (Bitmap)Bitmap.FromFile(ablak.FileNames[i2]);
                            bitmaps.Add(b);
                        }
                        ImageProcessor.EgeszKepesMuveletek.AVG(bitmaps);
                        Stopper.Stop();
                        times.Add(Stopper.ElapsedMilliseconds);
                        Stopper.Reset();
                    }

                    TesztEnd(times);
                }
                else
                {
                    Stopper.Start();

                    //Paths => Bitmaps
                    List<Bitmap> bitmaps = new List<Bitmap>();
                    for (int i = 0; i < ablak.FileNames.Length; i++)
                    {
                        Bitmap b = (Bitmap)Bitmap.FromFile(ablak.FileNames[i]);
                        bitmaps.Add(b);
                    }
                    info.LoadedImageExtension = System.IO.Path.GetExtension(ablak.FileNames[0]);
                    paramobj_outgoing.Image = ImageProcessor.EgeszKepesMuveletek.AVG(bitmaps);
                    TaskEnd();
                }
            }
        }
        private void MenuClick_EgeszKep_ADD(object sender, RoutedEventArgs e)
        {
            string path1 = KepBetoltoAblak();
            string path2 = KepBetoltoAblak();

            if (path1 == null || path2 == null)
                return;

            Bitmap b1 = (Bitmap)Bitmap.FromFile(path1);
            Bitmap b2 = (Bitmap)Bitmap.FromFile(path2);
            info.LoadedImageExtension = System.IO.Path.GetExtension(path1);


            if (info.TesztMod)
            {
                List<long> times = TesztStart();

                for (int i = 0; i < GlobalInfo.TesztekSzama; i++)
                {
                    Stopper.Start();
                    ImageProcessor.EgeszKepesMuveletek.ADD(b1, b2);
                    Stopper.Stop();
                    times.Add(Stopper.ElapsedMilliseconds);
                    Stopper.Reset();
                }

                TesztEnd(times);
            }
            else
            {
                Stopper.Start();
                paramobj_outgoing.Image = ImageProcessor.EgeszKepesMuveletek.ADD(b1, b2);
                TaskEnd();
            }
        }
        private void MenuClick_EgeszKep_SUB(object sender, RoutedEventArgs e)
        {
            string path1 = KepBetoltoAblak();
            string path2 = KepBetoltoAblak();

            if (path1 == null || path2 == null)
                return;

            Bitmap b1 = (Bitmap)Bitmap.FromFile(path1);
            Bitmap b2 = (Bitmap)Bitmap.FromFile(path2);
            info.LoadedImageExtension = System.IO.Path.GetExtension(path1);

            if (info.TesztMod)
            {
                List<long> times = TesztStart();

                for (int i = 0; i < GlobalInfo.TesztekSzama; i++)
                {
                    Stopper.Start();
                    ImageProcessor.EgeszKepesMuveletek.SUB(b1, b2);
                    Stopper.Stop();
                    times.Add(Stopper.ElapsedMilliseconds);
                    Stopper.Reset();
                }

                TesztEnd(times);
            }
            else
            {
                Stopper.Start();
                paramobj_outgoing.Image = ImageProcessor.EgeszKepesMuveletek.SUB(b1, b2);
                TaskEnd();
            }
        }
        #endregion

        #region Geometriai transzformációk
        private void MenuClick_Geom_Eltolas(object sender, RoutedEventArgs e)
        {
            if (Kep != null)
            {
                Ablakok.TwoDataWindow ablak = new Ablakok.TwoDataWindow("X: ", "Y: ");
                ablak.Data1LowerBound = int.MinValue;
                ablak.Data1UpperBound = int.MaxValue;
                ablak.Data1_OnlyInteger = true;
                ablak.Data1StartValue = 50;

                ablak.Data2LowerBound = int.MinValue;
                ablak.Data2UpperBound = int.MaxValue;
                ablak.Data2_OnlyInteger = true;
                ablak.Data2StartValue = 50;

                if (ablak.ShowDialog() == true)
                {
                    if (info.TesztMod)
                    {
                        List<long> times = TesztStart();

                        for (int i = 0; i < GlobalInfo.TesztekSzama; i++)
                        {
                            Stopper.Start();
                            ImageProcessor.GeometriaiTranszformaciok.Eltolas(KepMasolat, (int)ablak.Data1, (int)ablak.Data2);
                            Stopper.Stop();
                            times.Add(Stopper.ElapsedMilliseconds);
                            Stopper.Reset();
                        }

                        TesztEnd(times);
                    }
                    else
                    {
                        TaskStartImage();
                        paramobj_outgoing.Image = ImageProcessor.GeometriaiTranszformaciok.Eltolas(KepMasolat, (int)ablak.Data1, (int)ablak.Data2);
                        TaskEnd();
                    }
                }
            }
        }
        private void MenuClick_Geom_Atmeretezes(object sender, RoutedEventArgs e)
        {
            if (Kep != null)
            {
                Ablakok.TwoDataWindow ablak = new Ablakok.TwoDataWindow("Új szélesség: ", "Új magasság: ");
                ablak.Data1LowerBound = 1;
                ablak.Data1UpperBound = int.MaxValue;
                ablak.Data1_OnlyInteger = true;
                ablak.Data1StartValue = 50;

                ablak.Data2LowerBound = 1;
                ablak.Data2UpperBound = int.MaxValue;
                ablak.Data2_OnlyInteger = true;
                ablak.Data2StartValue = 50;

                if (ablak.ShowDialog() == true)
                {
                    if (info.TesztMod)
                    {
                        List<long> times = TesztStart();

                        for (int i = 0; i < GlobalInfo.TesztekSzama; i++)
                        {
                            Stopper.Start();
                            ImageProcessor.GeometriaiTranszformaciok.Atmeretezes(KepMasolat, (int)ablak.Data1, (int)ablak.Data2);
                            Stopper.Stop();
                            times.Add(Stopper.ElapsedMilliseconds);
                            Stopper.Reset();
                        }

                        TesztEnd(times);
                    }
                    else
                    {
                        TaskStartImage();
                        paramobj_outgoing.Image = ImageProcessor.GeometriaiTranszformaciok.Atmeretezes(KepMasolat, (int)ablak.Data1, (int)ablak.Data2);
                        TaskEnd();
                    }
                }
            }
        }
        private void MenuClick_Geom_Forgatas(object sender, RoutedEventArgs e)
        {
            if (Kep != null)
            {
                Ablakok.OneDataWindow ablak = new Ablakok.OneDataWindow("Hány fokkal? ");
                ablak.Data1LowerBound = -360;
                ablak.Data1UpperBound = 360;
                ablak.Data1_OnlyInteger = false;
                ablak.Data1StartValue = 90;

                if (ablak.ShowDialog() == true)
                {
                    if (info.TesztMod)
                    {
                        List<long> times = TesztStart();

                        for (int i = 0; i < GlobalInfo.TesztekSzama; i++)
                        {
                            Stopper.Start();
                            ImageProcessor.GeometriaiTranszformaciok.Forgatas(KepMasolat, (float)ablak.Data1);
                            Stopper.Stop();
                            times.Add(Stopper.ElapsedMilliseconds);
                            Stopper.Reset();
                        }

                        TesztEnd(times);
                    }
                    else
                    {
                        TaskStartImage();
                        paramobj_outgoing.Image = ImageProcessor.GeometriaiTranszformaciok.Forgatas(KepMasolat, (float)ablak.Data1);
                        TaskEnd();
                    }
                }
            }
        }
        #endregion

        #region Szűrők
        private void MenuClick_Szuro_Atlagolo(object sender, RoutedEventArgs e)
        {
            if (Kep != null)
            {
                Ablakok.OneDataWindow ablak = new Ablakok.OneDataWindow("Maszk mérete? (3/5/7/...)");
                ablak.Data1LowerBound = 3;
                ablak.Data1UpperBound = 21;
                ablak.Data1_OnlyInteger = true;
                ablak.Data1StartValue = 3;

                if (ablak.ShowDialog() == true)
                {
                    if (info.TesztMod)
                    {
                        List<long> times = TesztStart();

                        for (int i = 0; i < GlobalInfo.TesztekSzama; i++)
                        {
                            Stopper.Start();
                            ImageProcessor.Szurok.Atlagolo(KepMasolat, (int)ablak.Data1);
                            Stopper.Stop();
                            times.Add(Stopper.ElapsedMilliseconds);
                            Stopper.Reset();
                        }

                        TesztEnd(times);
                    }
                    else
                    {
                        TaskStartImage();
                        paramobj_outgoing.Image = ImageProcessor.Szurok.Atlagolo(KepMasolat, (int)ablak.Data1);
                        TaskEnd();
                    }
                }

            }
        }
        private void MenuClick_Szuro_Median(object sender, RoutedEventArgs e)
        {
            if (Kep != null)
            {
                Ablakok.OneDataWindow ablak = new Ablakok.OneDataWindow("Maszk mérete? (3/5/7/...)");
                ablak.Data1LowerBound = 3;
                ablak.Data1UpperBound = 21;
                ablak.Data1_OnlyInteger = true;
                ablak.Data1StartValue = 3;

                if (ablak.ShowDialog() == true)
                {
                    if (info.TesztMod)
                    {
                        List<long> times = TesztStart();

                        for (int i = 0; i < GlobalInfo.TesztekSzama; i++)
                        {
                            Stopper.Start();
                            ImageProcessor.Szurok.Median(KepMasolat, (int)ablak.Data1);
                            Stopper.Stop();
                            times.Add(Stopper.ElapsedMilliseconds);
                            Stopper.Reset();
                        }

                        TesztEnd(times);
                    }
                    else
                    {
                        TaskStartImage();
                        paramobj_outgoing.Image = ImageProcessor.Szurok.Median(KepMasolat, (int)ablak.Data1);
                        TaskEnd();
                    }
                }

            }
        }
        private void MenuClick_Szuro_Gauss(object sender, RoutedEventArgs e)
        {
            if (Kep != null)
            {
                Ablakok.TwoDataWindow ablak = new Ablakok.TwoDataWindow("Maszk mérete: ", "Szigma értéke: ");
                ablak.Data1LowerBound = 3;
                ablak.Data1UpperBound = 21;
                ablak.Data1_OnlyInteger = true;
                ablak.Data1StartValue = 3;

                ablak.Data2LowerBound = Double.Epsilon;
                ablak.Data2UpperBound = double.MaxValue;
                ablak.Data2_OnlyInteger = false;
                ablak.Data2StartValue = 1.0;

                if (ablak.ShowDialog() == true)
                {
                    if (info.TesztMod)
                    {
                        List<long> times = TesztStart();

                        for (int i = 0; i < GlobalInfo.TesztekSzama; i++)
                        {
                            Stopper.Start();
                            ImageProcessor.Szurok.Gauss(KepMasolat, (int)ablak.Data1, ablak.Data2);
                            Stopper.Stop();
                            times.Add(Stopper.ElapsedMilliseconds);
                            Stopper.Reset();
                        }

                        TesztEnd(times);
                    }
                    else
                    {
                        TaskStartImage();
                        paramobj_outgoing.Image = ImageProcessor.Szurok.Gauss(KepMasolat, (int)ablak.Data1, ablak.Data2);
                        TaskEnd();
                    }
                }
            }
        }
        private void MenuClick_Szuro_Elesites(object sender, RoutedEventArgs e)
        {
            if (Kep != null)
            {
                if (info.TesztMod)
                {
                    List<long> times = TesztStart();

                    for (int i = 0; i < GlobalInfo.TesztekSzama; i++)
                    {
                        Stopper.Start();
                        ImageProcessor.Szurok.ElesitesLaplace(KepMasolat);
                        Stopper.Stop();
                        times.Add(Stopper.ElapsedMilliseconds);
                        Stopper.Reset();
                    }

                    TesztEnd(times);
                }
                else
                {
                    TaskStartImage();
                    paramobj_outgoing.Image = ImageProcessor.Szurok.ElesitesLaplace(KepMasolat);
                    TaskEnd();
                }
            }
        }
        private void MenuClick_Szuro_HarrisSarokDetektor(object sender, RoutedEventArgs e)
        {
            if (Kep != null)
            {
                Ablakok.OneDataWindow ablak = new Ablakok.OneDataWindow("Threshold [x>0]:");
                ablak.Data1LowerBound = 1;
                ablak.Data1UpperBound = int.MaxValue;
                ablak.Data1_OnlyInteger = true;
                ablak.Data1StartValue = 1000;

                if (ablak.ShowDialog() == true)
                {
                    if (info.TesztMod)
                    {
                        List<long> times = TesztStart();

                        for (int i = 0; i < GlobalInfo.TesztekSzama; i++)
                        {
                            Stopper.Start();
                            ImageProcessor.Szurok.HarrisSarokDetektor(KepMasolat, (int)ablak.Data1);
                            Stopper.Stop();
                            times.Add(Stopper.ElapsedMilliseconds);
                            Stopper.Reset();
                        }

                        TesztEnd(times);
                    }
                    else
                    {
                        TaskStartImage();
                        paramobj_outgoing.Image = ImageProcessor.Szurok.HarrisSarokDetektor(KepMasolat, (int)ablak.Data1);
                        TaskEnd();
                    }
                }
            }
        }
        #endregion

        #region Beállítások
        private void MenuClick_Beallitasok_TesztModChange(object sender, RoutedEventArgs e)
        {
            info.TesztMod = ((System.Windows.Controls.MenuItem)sender).IsChecked;
        }
        private void MenuClick_Beallitasok_TesztekSzamanakBeallitasa(object sender, RoutedEventArgs e)
        {
            Ablakok.OneDataWindow ablak = new Ablakok.OneDataWindow("Tesztek száma? [n > 1]");
            ablak.Data1LowerBound = 2;
            ablak.Data1UpperBound = int.MaxValue;
            ablak.Data1_OnlyInteger = true;
            ablak.Data1StartValue = GlobalInfo.TesztekSzama;

            if (ablak.ShowDialog() == true)
            {
                GlobalInfo.TesztekSzama = (int)ablak.Data1;
            }
        }

        #endregion

        #region Súgó
        private void MenuClick_Nevjegy(object sender, RoutedEventArgs e)
        {
            MessageBox.Show(GlobalInfo.Nevjegy, "ArphoxDIP infó", MessageBoxButton.OK, MessageBoxImage.Information);
        }
        #endregion
    }
}