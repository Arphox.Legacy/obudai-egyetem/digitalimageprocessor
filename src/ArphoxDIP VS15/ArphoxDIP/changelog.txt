﻿==================================================================================================
Verzió 1.0
------------------------------
Új dolgok:
+ GetPixel-es képtípus meghatározás átírva unsafe kódra.
~ A pozíción lévő kép pixelek színének meghatározását is megírtam unsafe kódban de felesleges azt
	használni, mert nem gyorsít. (Mindkettő < 1ms ...)
+ Optimalizálás:
	1.
	int W = imageOut.Width; int H = imageOut.Height; sorok felvéve mindenhol gyorsítás érdekében.
	2.
	A Tanár úr javaslata alapján a ciklusmagokban lévő imageInData.Stride sorok is kirakva lokális
	változóba.
+ TESZT MÓD! Állítsd be a beállításokban a tesztek számát (N), pipáld be a teszt módot (vagy nyomd
	meg	a T betűt), futtass le egy műveletet és a program elvégzi az adott műveletet N-szer, majd
	kiírja az idők átlagát. Amíg teszt módban vagy, a háttér más színű :)

Generált bugok: 0

Megjegyzés:
Nagyon tetszik ez a teszt mód :)
Ennyi időm volt erre a beadandóra, az elején nagyon sokat szívtam a processes témával, és nagyon
lemaradtam; így az optimalizálásra szánt idő elment a szívással. Nem baj, örülök hogy ennyit
tudtam tanulni és összedobni egy ilyen szuper programot :)
==================================================================================================
Verzió 0.20 - 2015.10.20 18:45
------------------------------
Új dolgok:
+	Laplace élesítő

Generált bugok: 0

Megjegyzés:
	Éééés beértem magam a teendőkkel. Hálistennek jól működik minden, mehetek assemblyzni...
==================================================================================================
Verzió 0.14 - 2015.10.20 14:59
------------------------------
Új dolgok:
+	Átlagoló szűrő
+	Medián szűrő
+	Gauss szűrő

Generált bugok: 0

Megjegyzés:
	Végre sikerült n×n-es esetre megcsinálni a szűrőket, örülök hogy sikerült. Jó pár órányi gon-
dolkodásból születet. Hátravan még az élesítés, de azt most már nincs kedvem megcsinálni.
==================================================================================================
Verzió 0.13 - 2015.10.20 01:30
------------------------------
Új dolgok:
+	Egér görgővel most már zoomolható a kép. Statusbar zoomolt képnél nem jó (így eltűntetem).
	A görgetés mértéke állítható a GlobalInfo-ban.(ZoomMertek property)
+	Egér görgő megnyomásakor a Zoom az alapértelmezett értékre vált (0).
+	Nyíl billentyűkkel a scrollvieweren belül 10%-ot ugrik az adott irányba a csúszka.

Generált bugok:
	a statusbar nem megy zoomolt képnél

Megjegyzés:
	A statusbar nem megy a zoomolt képnél mert skálázgatni kéne ahhoz pedig már túl fáradt vagyok.
De igazából szarok rá, mert a második leadáshoz még nincsenek meg a szűrések, azt kell megcsinálni
csak eszembe jutott zuhanyozás közben, hogy ezt így meg lehetne csinálni viszonylag gyorsan. :D
==================================================================================================